;;; anarcat/init -- anarcat's .emacs file
;;
;; -*- mode: emacs-lisp; -*-
;;
;;; Commentary:
;;
;; each section of this file is seperated by page breaks (^L's)
;;
;; you can use narrow-to-page (C-x-n-p) to navigate through it, but
;; there is a nicer way if you load this file: C-x C-[ for prev and
;; C-x C-] for next page.
;;
;; Others simply split their very long init file in multiple modules,
;; so far I've kept it as a single file except for the early init,
;; custom, and some package specific stuff (e.g. notmuch has its own
;; config file).  I've tried splitting it up, but then linters
;; complain about functions (e.g. use-package) not being
;; defined.  Probably me failing at elisp again.
;;
;;; Code:


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; legacy code that's not in packages
(add-to-list 'load-path "~/.emacs.d/elisp/")

;; Make startup faster by reducing the frequency of garbage
;; collection.  The default is 800 kilobytes.  Measured in bytes.
;; this is 50 megabytes
(setq gc-cons-threshold (* 50 1000 1000))

;; reduce native compilation warnings
(setq byte-compile-warnings '(not obsolete))
(setq warning-suppress-log-types '((comp) (bytecomp)))
(setq native-comp-async-report-warnings-errors 'silent)

;; this fixes the "bad request" error on melpa, must be removed in
;; Emacs 27 or later
(when (< emacs-major-version 27)
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

;; configure the package repositories
(when (require 'package nil t)
  (setq-default load-prefer-newer t)
  (when (< emacs-major-version 27)
    ;; this is done automatically in Emacs 27 and later, between
    ;; early-init.el and init.el
    (setq-default package-enable-at-startup nil)
    (package-initialize))
  (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                           ("melpa" . "https://melpa.org/packages/")))
  (with-demoted-errors "%s"
    ;; in elpa-use-package debian package since stretch
    (unless (package-installed-p 'use-package)
      (package-refresh-contents)
      (package-install 'use-package))))

;; function to upgrade all packages automatically
;;
;; fired from a systemd timer in ~/.config/systemd/user/emacs-update.service
;;
;; cargo-culted from https://emacs.stackexchange.com/a/4052/11929
;; and https://github.com/Malabarba/paradox/blob/339fe3518d1d102b2295670340e75caf4f01a29a/paradox.el#L165
(defun anarcat/upgrade-packages ()
  "Upgrade all packages.  No questions asked.
This function is equivalent to `list-packages', followed by a
`package-menu-mark-upgrades' and a `package-menu-execute'.  Except
the user isn't asked to confirm deletion of packages."
  (save-window-excursion
    (list-packages)
    (package-menu-mark-upgrades)
    (condition-case err
        (package-menu-execute 'no-query)
      (user-error
       (message "ignoring error: %s" (error-message-string err))))))

;; setup the use-package package to configure all the remaining
;; packages below.
(eval-when-compile
  (require 'use-package)
  (setq-default
   use-package-always-defer nil
   use-package-always-ensure nil))

;; use this to, well, benchmark the init file (.emacs)
;;(use-package benchmark-init
;;  :config
;;  ;; To disable collection of benchmark data after init is done.
;;  (add-hook 'after-init-hook 'benchmark-init/deactivate))
;; use one of those functions to show the results
;; (benchmark-init/show-durations-tabulated)
;; (benchmark-init/show-durations-tree)

;; allow web servers to open edition buffers so i can edit text areas
;; in Emacs
;;(use-package atomic-chrome
;;  :defer 1
;;  :init (add-hook 'after-init-hook 'atomic-chrome-start-server))

;;; Emacs expects .emacs to load customizations
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROJECT MANAGEMENT AND COMPLETION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; this is extremely confusing. there are many packages to do various
;; things a "modern IDE" does, and while Emacs itself provides some of
;; it, even there there are multiple alternatives, some incompatible
;; with each other.
;;
;; let's define the problem space. we have:
;;
;; 1. projects
;; 2. in-buffer completion
;; 3. minibuffer completion
;;
;; First, we have the notion of "a project" which I will simplify as
;; being basically "a repository where we can easily find file, do a
;; grep over all (relevant) files, compile, test, etc". This is
;; currently managed by project.el, shipped with Emacs 28.
;;
;; Second we have in-buffer completion. This is what happens when you
;; type TAB or M-/ in a buffer to (say) complete a symbol. LSP might
;; conflict with this. For this I'm currently using corfu.
;;
;; Third we have minibuffer completions. This is what happens when you
;; hit (say) M-x or C-x C-f and then TAB around. For this we use
;; vertico to display results vertically and marginalia to show more
;; information. completion-styles are what define how the string
;; before TAB is actually parsed.
;;
;; So let's take this one by one.

;; 1. find files in projects
;;
;; in my shell, i have CDPATH set to (basically)
;; "src:dist:wikis:src/tor" to quickly jump to some directories. i
;; have been looking for an alternative for that in Emacs for a long
;; time, and it seems project.el is a pretty good approximation.
;;
;; this, of course, does a *lot* more like have an easy "jump to
;; another file in this project" or "save all files in this
;; project". and it's not perfect, but it's a great start.
;;
;; I previously used projectile here but the builtin project.el has
;; progressed quite a bit, and seems more mature in Emacs 28, so I
;; have stopped using projectile altogether. The maintainer of
;; projectile Debian package abandoned it in favor of project.el in
;; 2022:
;;
;; https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1003031
(when (>= emacs-major-version 28)
  (with-eval-after-load "project"
    (message "refreshing project list...")
    (anarcat/project-discover-projects-in-search-path)
    (project-forget-zombie-projects)))

(defun anarcat/project-discover-projects-in-search-path ()
  "Automatically discover projects in `anarcat/projects-autodiscover'.

This is a helper function for bootstrapping a project list.  It's
designed to be ran at startup or rarely."
  (let ((project-discovered-count
         (apply '+
                (mapcar
                 #'project-remember-projects-under
                 anarcat/project-search-path)))
        (project-actual-count
         (length project--list)))
    (message "%d projects identified out of %d searched" project-actual-count project-discovered-count)))

(defvar anarcat/project-search-path
  '("~" "~/wikis" "~/src" "~/src/tor")
  "List of directories to inspect for new projects.

This is used in `anarcat/project-discover-projects-in-search-path'.")

(use-package rg
  :defer t
  :bind ([f8] . rg-project))

;; recentf keeps
;; recentf is useful inside projectile and, in general
;;
;; i have previously switched to "session" mode, but it seems this is
;; better integrated now. and besides i don't really want "sessions",
;; i just want to be able to jump around projects and find files
;; easily. this will give me a nice little history of recent files.
(use-package recentf
  :init
  (recentf-mode 1)
  :custom
  ;; keep more history: bump from 20 to 1000
  (recentf-max-saved-items 1000))

;; 2. in-buffer autocompletion: corfu
;;
;; https://github.com/minad/corfu
;;
;; corfu provides a popup to show possible completion after a timeout.
;;
;; This handles completion-in-region or completion-at-point ("TAB" or
;; timeout in a buffer). This is the in-buffer counterpart to
;; minibuffer completion (vertico). We picked it because it's simpler
;; than Company and uses the standard Emacs completion API ("capf",
;; `completion-at-point-function'), while company uses its own thing
;; (but can also use capf).
;;
;; corfu is packaged as elpa-corfu in Debian.
;;
;; see also this evaluation
;; https://utcc.utoronto.ca/~cks/space/blog/programming/EmacsUnderstandingCompletion
;;
;; alternatives include the built-in `icomplete-in-buffer' or the
;; `*Completions*' buffer. Emacs 30 will also ship with a new
;; `completion-preview.el' library. `company' and
;; https://code.bsdgeek.org/adam/corfu-candidate-overlay also provide
;; third-party replacements, see also:
;;; https://eshelyaron.com/posts/2023-11-17-completion-preview-in-emacs.html
(use-package corfu
  :defer 1
  :init
  (global-corfu-mode)
  :custom
  ;; Select the first candidate, except for directories
  (corfu-preselect 'directory)
  ;; autocomplete without keybinding, 0.2s delay by default
  (corfu-auto-delay 0.5)
  (corfu-auto t))

;; dabbrev completes words based on words already present in the
;; buffer (or related buffers with a prefix, or *all* buffers with
;; two).
;;
;; config from corfu
(use-package dabbrev
  ;; Swap M-/ and C-M-/
  :bind (("M-/" . dabbrev-completion)
         ("C-M-/" . dabbrev-expand))
  :config
  (add-to-list 'dabbrev-ignored-buffer-regexps "\\` ")
  (if (< emacs-major-version 29)
      (progn
        (add-to-list 'dabbrev-ignored-buffer-regexps ".[pP][dD][fF]"))
    (progn
      (add-to-list 'dabbrev-ignored-buffer-modes 'doc-view-mode)
      (add-to-list 'dabbrev-ignored-buffer-modes 'pdf-view-mode))))

;; cape provides a handful of convenient capf
;; (completion-at-point-function) that are, in turn, used by corfu.
;;
;; not in Debian
(use-package cape
  :ensure t
  :defer 1
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("C-c p p" . completion-at-point) ;; capf
         ("C-c p t" . complete-tag)        ;; etags
         ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c p h" . cape-history)
         ("C-c p f" . cape-file)
         ("C-c p k" . cape-keyword)
         ("C-c p s" . cape-elisp-symbol)
         ("C-c p e" . cape-elisp-block)
         ("C-c p a" . cape-abbrev)
         ("C-c p l" . cape-line)
         ("C-c p w" . cape-dict)
         ("C-c p :" . cape-emoji)
         ("C-c p \\" . cape-tex)
         ("C-c p _" . cape-tex)
         ("C-c p ^" . cape-tex)
         ("C-c p &" . cape-sgml)
         ("C-c p r" . cape-rfc1345))
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  ;;(add-to-list 'completion-at-point-functions #'cape-history)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;;(add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  (add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;;(add-to-list 'completion-at-point-functions #'cape-elisp-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
  ;;
  ;; also possible to use company as a backend, see
  ;; https://github.com/minad/cape?tab=readme-ov-file#company-adapter
)

;; other alternatives to corfu
;;
;; company: was previously used because it supports multiple
;; completion backends and has popups.
;;
;; auto-complete-mode also has popups, but is not as flexibles or
;; lightweight, and doesn't currently work in emacs25
;;
;; I have also previously used pabbrev, dabbrev, predictive, and
;; hippie-expand. none of those are as good and flexible as company,
;; but now I wonder if company might not overlap with LSP...

;; other improvements
;;
;; orderless: https://github.com/oantolin/orderless an additional
;; "completion style" that "divides the pattern into space-separated
;; components, and matches candidates that match all of the components
;; in any order. Each component can match in any one of several ways:
;; literally, as a regexp, as an initialism, in the flex style, or as
;; multiple word prefixes". See also this review:
;; https://utcc.utoronto.ca/~cks/space/blog/programming/EmacsUnderstandingOrderless

;; interwiki-style completions for markdown buffers. ie. turn wp:Emacs
;; into [Emacs](https://en.wikipedia.org/wiki/Emacs) and
;; gl:tpo/tpa/team#1234 into
;; [tpo/tpa/team#1234](https://gitlab.torproject.org/tpo/tpa/team/-/issues/1234)
;;
;; according to johnw in #emacs, this can be done with hippie-expand,
;; but i worry it might not work with corfu (or company, of
;; course). minad (corfu's author) argues hippie is deprecated in
;; https://github.com/minad/cape/issues/42#issuecomment-1131993675 and
;; suggests capf instead.
;;
;; docs on hippie-expand usage:
;; https://www.masteringemacs.org/article/text-expansion-hippie-expand
;;
;; docs on how to make new expanders for hippie-expand in
;; hippie-exp.el, not well documented. but it seems to be to make a
;; `try` function and append it to
;; hippie-expand-try-functions-list. then hippie-exp.el explains how
;; to actually build the function.
;;
;; inspiration for this:
;; https://news.ycombinator.com/item?id=35604246
;;
;; background for the InterWiki notion:
;;
;; http://meatballwiki.org/wiki/InterWiki
;;
;; sources for this list:
;; https://ikiwiki.info/shortcuts/
;;
;; other sources, not used:
;; http://meatballwiki.org/wiki/InterMapTxt
;; https://en.wikipedia.org/wiki/Help:Interwiki_linking
;; https://moinmo.in/InterWiki
;; https://www.mediawiki.org/w/api.php?action=query&meta=siteinfo&siprop=interwikimap
;;
;; this was made to save time while writing those links, but it's
;; quite likely more time was spent to figure out how to implement
;; this than the actual time saved. at about 8 hours so far.
;;
;; originally, I intenteded to make some autocompletion mechanism that
;; would "pop up" a completion when typing interwiki. This turned out
;; to be impractical: from what I have gathered, the
;; `completion-at-point-functions' (or capf) return a list of matches,
;; and those need to match the original string directly. So
;; "wikipedia" can expand to "wikipedia.org/wiki" but "wp" cannot, and
;; even less "wp:Foo". There *might* be a way to do some workarounds
;; with the `:exit-function' stuff but at that point we're really
;; hacking around capf limitations more than anything, and I wasted
;; too much time on this.
(defvar interwiki-dict
  '(
    ("archive" . "http://web.archive.org/*/")
    ("arxiv" . "http://arxiv.org/abs/")
    ("c2" . "http://wiki.c2.com/?")
    ("cpan" . "http://search.cpan.org/search?mode=dist&query=")
    ("cpanrt" . "https://rt.cpan.org/Ticket/Display.html?id=")
    ("ctan" . "http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=")
    ("cve" . "https://cve.mitre.org/cgi-bin/cvename.cgi?name=")
    ("debbug" . "http://bugs.debian.org/")
    ("debcve" . "https://security-tracker.debian.org/tracker/")
    ("deblist" . "https://lists.debian.org/debian-")
    ("debmsg" . "https://lists.debian.org/msgid-search/")
    ("debpkg" . "http://packages.debian.org/")
    ("debpkgsid" . "http://packages.debian.org/sid/")
    ("debpts" . "http://packages.qa.debian.org/")
    ("debrt" . "https://rt.debian.org/Ticket/Display.html?id=")
    ("debss" . "http://snapshot.debian.org/package/")
    ("debwiki" . "https://wiki.debian.org/")
    ("dict" . "http://www.dict.org/bin/Dict?Form=Dict1&Strategy=*&Database=*&Query=")
    ("doi" . "http://dx.doi.org/")
    ("emacswiki" . "http://www.emacswiki.org/cgi-bin/wiki/")
    ("fdobug" . "https://bugs.freedesktop.org/show_bug.cgi?id=")
    ("fdolist" . "http://lists.freedesktop.org/mailman/listinfo/")
    ("flickr" . "https://secure.flickr.com/photos/")
    ("freebsdwiki" . "http://wiki.freebsd.org/")
    ("gmane" . "http://dir.gmane.org/gmane.")
    ("gmanemsg" . "http://mid.gmane.org/")
    ("gmap" . "https://maps.google.com/maps?q=")
    ("gmsg" . "https://groups.google.com/groups?selm=")
    ("gnomebug" . "https://bugzilla.gnome.org/show_bug.cgi?id=")
    ("gnulist" . "https://lists.gnu.org/mailman/listinfo/")
    ("google" . "https://encrypted.google.com/search?q=")
    ("gpg" . "http://pgpkeys.mit.edu:11371/pks/lookup?op=vindex&exact=on&search=0x")
    ("hackage" . "http://hackage.haskell.org/package/")
    ("haskellwiki" . "http://haskell.org/haskellwiki/")
    ("hoogle" . "http://haskell.org/hoogle/?q=")
    ("iki" . "http://ikiwiki.info/")
    ("imdb" . "http://imdb.com/find?q=")
    ("linuxbug" . "https://bugzilla.kernel.org/show_bug.cgi?id=")
    ("man" . "http://manpages.debian.org/")
    ("marclist" . "http://marc.info/?l=")
    ("marcmsg" . "http://marc.info/?i=")
    ("meatballwiki" . "http://www.usemod.com/cgi-bin/mb.pl?")
    ("mozbug" . "https://bugzilla.mozilla.org/show_bug.cgi?id=")
    ("mozillazinekb" . "http://kb.mozillazine.org/")
    ("novellbug" . "https://bugzilla.novell.com/show_bug.cgi?id=")
    ("ohloh" . "https://www.ohloh.net/p/")
    ("perldoc" . "http://perldoc.perl.org/search.html?q=")
    ("pkgsrc" . "http://pkgsrc.se/")
    ("rfc" . "https://datatracker.ietf.org/doc/html/rfc")
    ("tpawiki" . "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/")
    ("ubupkg" . "http://packages.ubuntu.com/")
    ("whois" . "http://reports.internic.net/cgi/whois?type=domain&whois_nic=")
    ("wikipedia" . "https://en.wikipedia.org/wiki/")
    ("wikitravel" . "https://wikitravel.org/en/")
    ("wiktionary" . "https://en.wiktionary.org/wiki/")
    ("wp" . "https://en.wikipedia.org/wiki/")
    ))

;; TODO: this needs to returns a list of possible matching keys only,
;; then an :annotation-function that turns the prefix into the url,
;; and finally an :exit-function that does the replacement, at least
;; that is how cape-abbrev does its magic.
(defun interwiki-slug-to-url-complete-at-point ()
  (interactive)
  (when-let ((bounds (if (use-region-p) (cons (region-beginning) (region-end))
                    (bounds-of-thing-at-point 'filename)))
             (match (buffer-substring-no-properties (car bounds) (cdr bounds)))
             (url (interwiki-slug-to-url match))
             (ret (list (car bounds) (cdr bounds) (list url))))
    (message "bds %s url %s ret %s" bounds url ret)
    ret))

;;(add-to-list 'completion-at-point-functions #'interwiki-slug-to-url-complete-at-point)

(defun interwiki-slug-to-url (slug)
  "Turn the given interwiki SLUG into a URL.

This should return http://example.com/foo for example:foo,
given `(\"example\" . \"http://example.com\")' in the
`interwiki-dict'."
  (when-let ((hunk (split-string slug ":"))
             (key (pop hunk))
             (value (pop hunk))
             (urlmap (assoc key interwiki-dict)))
    (concat (cdr urlmap) value)))

(defun interwiki-slug-to-url-dwim ()
  "Expand the given interwiki pattern into a full markdown link.

This will expand foo:bar into [bar](example.com/foo) if
`interwiki-dict' has (bar . example.com) in its list."
  (interactive)
  (when-let ((bounds (if (use-region-p) (cons (region-beginning) (region-end))
                    (bounds-of-thing-at-point 'filename)))
             (match (buffer-substring-no-properties (car bounds) (cdr bounds)))
             (url (interwiki-slug-to-url match)))
    (delete-region (car bounds) (cdr bounds))
    (insert url)))

;; GitLab URLs rewriting code
;;
;; Related:
;; https://github.com/nlamirault/emacs-gitlab
;; https://github.com/isamert/lab.el
;;
;; Both allow browsing issues, projects, milestones and so on from
;; Emacs, sometimes also modifying, creating, closing or watching
;; objects as well. They don't provide this level of completion though.
;;
;; https://github.com/sshaw/git-link/
;;
;; provides a way to extract a web link for the current source file,
;; something completely different...

;; URL for the GitLab side for `gitlab-url-mdwn-expand-dwim' and friends.
(setq gitlab-url "https://gitlab.torproject.org")
;; map of symbol -> keyword for GitLab URLs
;;
;; from https://docs.gitlab.com/ee/user/markdown.html#gitlab-specific-references
(defvar gitlab-ref-alis
  '(
    ("#" . "issues")
    ("!" . "merge_requests")
    ("%" . "milestones")
    ("@" . "commit")
    ("$" . "snippets")
    ))

;; TODO This is not hooked into `completion-at-point-functions'
;; because, like `interwiki-complete-at-point', it doesn't work (or at
;; least we assume it doesn't, untested).
(defun gitlab-slug-to-url-dwim ()
  "Replace the given GitLab slug into a URL.

If region is active, it uses that for the scope of the
completion, otherwise it uses `bounds-of-thing-at-point' with a
'filename' argument.

This turns foo#1 into `gitlab-url'/foo/issues/-/.  Similar to
`gitlab-slug-to-url-mdwn-dwim', but without the Markdown."
  (interactive)
  (when-let ((bounds (if (use-region-p) (cons (region-beginning) (region-end))
                       (bounds-of-thing-at-point 'filename)))
             (match (buffer-substring-no-properties (car bounds) (cdr bounds)))
             (url (gitlab-slug-to-url match)))
    (delete-region (car bounds) (cdr bounds))
    (insert url)))

(defun gitlab-slug-to-url-mdwn-dwim ()
  "Replace the given GitLab reference with an actual Markdown link.

If region is active, it uses that for the scope of the
completion, otherwise it uses `bounds-of-thing-at-point' with a
'filename' argument.

This turns foo#1 into [foo#1](`gitlab-url'/foo/issues/-/1).
Similar to `gitlab-slug-to-url-dwim' but with a Markdown link."
  (interactive)
  (when-let ((bounds (if (use-region-p) (cons (region-beginning) (region-end))
                       (bounds-of-thing-at-point 'filename)))
             (match (buffer-substring-no-properties (car bounds) (cdr bounds)))
             (url (gitlab-slug-to-url match)))
    (delete-region (car bounds) (cdr bounds))
    (insert (format "[%s](%s)" match url))))

(defun gitlab-slug-to-url (slug)
  "Turn the given `SLUG' into a GitLab URL.

The URL is derived from `gitlab-url' and the `gitlab-ref-alis'."
  (when slug
    (when-let (;; extract separators from the map
               (gitlab-symbols (apply 'concat (mapcar 'car gitlab-ref-alis)))
               ;; build a regex to split on seperators
               (re (concat "\\(.*\\)\\([" gitlab-symbols "]\\)\\(.*\\)"))
               ;; apply the regex
               (seppos (string-match re slug))
               ;; first group is project, then separator, then the value
               (project (match-string 1 slug))
               (sep (match-string 2 slug))
               (value (match-string 3 slug))
               ;; lookup matching keyword in map
               (keyword (cdr-safe (assoc sep gitlab-ref-alis))))
      (format "%s/%s/-/%s/%s" gitlab-url project keyword value))))

;; for `url-generic-parse-url' below
(require 'url)

(defun gitlab-url-to-slug (url)
  "Turn the given `url' into a GitLab slug.

This looks for something that looks like a URL with
`url-generic-parse-url' then look for the magic GitLab
separator ('/-/') to find the project, keyword and value.

So we're looking for something that looks like
PROJECT/-/KEYWORD/VALUE, e.g. anarcat/test/-/issues/1 and turn
that into (e.g.) anarcat/test#1 based on the `gitlab-ref-alis'."
  (when-let ((urlobj (url-generic-parse-url url))
             (path (url-filename urlobj)) ;; path is what comes after the hostname in the URL
             (parts (split-string path "/-/"))
             (project (string-trim (car parts) "/")) ;; left side is the project name
             (second (split-string (cadr parts) "/")) ;; right side needs splitting again
             (keyword (car second)) ;; keyword is "issues" in the above example.com/foo
             (value (cadr second)) ;; value would be "1" above
             (symbol (car (rassoc keyword gitlab-ref-alis)))) ;; lookup "issues" to "#" in the map
        (concat project symbol value)))

(defun gitlab-url-to-slug-dwim ()
  "Convert a long GitLab URL into its short slug form.

This turns, for example,
'https://example.com/anarcat/test/-/issues/1' into
anarcat/test#1. It does *not* look for the `gitlab-url' and
will work regardless of the GitLab instance, as long as there's
that magic '/-/' bit."
  (interactive)
  (when-let ((bounds (if (use-region-p) (cons (region-beginning) (region-end))
                       (bounds-of-thing-at-point 'url)))
             (match (buffer-substring-no-properties (car bounds) (cdr bounds)))
             (slug (gitlab-url-to-slug match)))
    (delete-region (car bounds) (cdr bounds))
    (insert slug)))

(defun gitlab-url-to-slug-complete-at-point ()
  "Complete the URL found into a GitLab slug, if found."
  ;; TODO: couldn't figure out how to merge this with
  ;; `gitlab-url-to-slug-dwim', but maybe it could be merged with the
  ;; reverse capf? `gitlab-url-to-slug-dwim` does call
  ;; `gitlab-url-to-slug-complete-at-point' but it's not much prettier
  ;; than this, and in fact almost as long as the original.
  (when-let ((bounds (if (use-region-p) (cons (region-beginning) (region-end))
                       (bounds-of-thing-at-point 'url)))
             (match (buffer-substring-no-properties (car bounds) (cdr bounds)))
             (slug (gitlab-url-to-slug match)))
    (append bounds (list slug))))

;; 3. minibuffer completion
;;
;; vertico enables vertical completion mode, which turns the
;; mini-buffer autocomplete into a vertical display
(use-package vertico
  :init
  (vertico-mode)
  :custom
  (vertico-cycle t)
  ;; revert back to keeping the prompt selected by default, instead of
  ;; the first candidate
  ;;
  ;; this is something i find very confusing with those alternative
  ;; minibuffer autocompletion, this manic tendency of trying to
  ;; optimize defaults. it particularly wrecks havoc when working in
  ;; Markdown links, where the first option for the link target is
  ;; generally *not* what we want, and worse, if we paste a URL that
  ;; is similar to an existing one, we are likely to pick the existing
  ;; one.
  ;;
  ;; thankfully, we can disable this.
  (vertico-preselect 'prompt)
  ;; change completion style
  ;;
  ;; partial-completion does some sort token-separated search,
  ;; turning, say, "f-o-o" into "food-over-obscurity" and also
  ;; "/u/s/d" could turn into "/usr/share/doc"
  ;;
  ;; substring is the simplest form, matching "foo" to "foobar"
  ;;
  ;; flex is a little more obscure and tokenizes every character, so
  ;; that "foo" can complete to "frodo"
  ;;
  ;; The default is '(basic partial-completion emacs22), full
  ;; documentation is in `completion-styles-alist' and the Info node
  ;; `(emacs)Completion Styles `(elisp)Font Lock Basics'.
  (completion-styles '(partial-completion substring flex)))

;; enhance vertico: show defails of completion candidates
(use-package marginalia
  ;; Either bind `marginalia-cycle' globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

;; search and navigation tools
;;
;; I have previously used "consul" as well but stopped using it
;; because it was so invasive (it was basically taking over *all*
;; keybindings by default). Instead here we pick and choose a smaller
;; subset. It is also our hope that consult is generally less invasive
;; than counsel and more modular.
(use-package consult
  ;; part of the suggested keybindings, way more options in
  ;; https://github.com/minad/consult#use-package-example
  :bind
  (("C-c m" . consult-man)
   ("C-c i" . consult-info)
   ([remap Info-search] . consult-info)
   ("M-g g" . consult-goto-line)             ;; orig. goto-line
   ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
   ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
   ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
   ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
   ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
   ("M-g i" . consult-imenu)
   ;; M-s bindings (search-map)
   ("M-s d" . consult-find)
   ("M-s D" . consult-locate)
   ("M-s g" . consult-grep)
   ("M-s G" . consult-git-grep)
   ("M-s r" . consult-ripgrep)
   ("M-s l" . consult-line)
   ("M-s L" . consult-line-multi)
   ("M-s k" . consult-keep-lines)
   ("M-s u" . consult-focus-lines)
   ;; Isearch integration
   ("M-s e" . consult-isearch-history))
  :custom
  (consult-preview-key "M-."))

;; act on a completion list, e.g. dump the entire list to a buffer,
;; etc.
(use-package embark
  :bind
  (("C-." . embark-act)    ;; prompt for multiple options
   ("C-e" . embark-export))) ;; export completion buffer on its own
;; not sure what this is for.
(use-package embark-consult
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; I previously used the builtin icomplete-mode to enhance minibuffer
;; completion, which served me quite well, but then I switched to
;; fido, ido, and then the above vertico.
;;
;; fido-mode is similar to icomplete-mode, except it supports C-s C-r
;; and C-k to kill buffers. I can't figure out why I have abandoned it
;; in favor of ido, but I do know vertico feels much more intuitive,
;; or at least doesn't disrupt as much the normal operation of the
;; minibuffer (e.g. ENT must pick the selected entry, not enter a
;; folder!)
;;
;; I have also previously used ido here, but it's deprecated in Emacs
;; 28, see: http://xahlee.info/emacs/emacs/emacs_ido_mode.html
;;
;; I ultimately abandoned ido in favor of vertico because ido was
;; pretty confusing: it would enter a directory instead of selecting
;; the directory, and I had activated those weird "follow url"
;; features which I would constantly trip on and never use.
;;
;; I am not using ivy here either, after trying it at least twice. See
;; commit 451a861 (turn off ivy and go back to builtin icomplete-mode,
;; 2022-04-08) for details.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROGRAMMING SUPPORT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TODO
;; evaluate: smartparens (not in debian)

;; language server support
;;
;; Language Server Protocol (LSP) provides a standard way (across
;; programming languages) to provide autocompletion and other kind of
;; suggestions (like ordering imports with isort in Python, or
;; reformatting source code with gofmt in Golang).
;;
;; I was previously using lsp-mode here, but abandoned it after having
;; serious hangs and latency issues with LSP. I also couldn't figure
;; out how to hook up Harper. I had followed this guide then:
;;
;; https://ianyepan.github.io/posts/emacs-ide/
;;
;; Now we use eglot here instead, as it's in core and seems more
;; lightweight and has most of what we need here, apparently, except
;; the heading which I do not use. but if I would, I could use this
;; instead:
;;
;; https://github.com/joaotavora/breadcrumb
;;
;; for golang, there's golsp installed by puppet. for Python, there's
;; pylsp, but apparently pyright can do this and so can:
;;
;; https://github.com/pappasam/jedi-language-server
;;
;; configuration here inspired by
;; https://andreyor.st/posts/2023-09-09-migrating-from-lsp-mode-to-eglot/
;; https://justinbarclay.ca/posts/from-zero-to-ide-with-emacs-and-lsp/
;;
;; manual: https://joaotavora.github.io/eglot/
;; curiously not in the Emacs manual?
;;
;; note that we don't have DAP (Debugging Adapter Protocol) setup
;; yet. dap-mode is apparently tightly bound to lsp-mode, a better
;; option would likely be https://github.com/svaante/dape
(use-package eglot
  :defer
  :hook
  (markdown-mode . flyspell-mode)
  ;; disable generic LSP on programming mode, as that's a security issue.
  ;; https://eshelyaron.com/posts/2024-11-27-emacs-aritrary-code-execution-and-how-to-avoid-it.html
  ;; https://security-tracker.debian.org/tracker/CVE-2024-53920
  ;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=37656
  ;;(prog-mode . eglot-ensure)
  :bind (:map
         eglot-mode-map
         ("C-c e r" . eglot-rename)
         ("C-c e a" . eglot-code-actions)
         ("C-c e o" . eglot-code-action-organize-imports)
         ("C-c e d" . eldoc)
         ("C-c e f" . eglot-format)
         ("C-c e =" . eglot-format))
  :config
  (setq-default eglot-workspace-configuration
                ;; disable pycodestyle by default, i don't like its style
                '(:pylsp (:plugins (:pycodestyle (:enabled :json-false)))))
  :custom
  (eglot-autoshutdown t) ;; default is to leave servers runing when
                         ;; last buffer exits
  (eglot-events-buffer-size 0) ;; disable events recording to speed
                               ;; things up, comment out to
                               ;; troubleshoot and look for the EGLOT
                               ;; buffer
  (eglot-extend-to-xref nil)) ;; cover files found through xref (M-.)

;; allows one to have multiple cursors (e.g. "point" in Emacs) to edit
;; multiple lines (or symbols) at once
;;
;; an alternative is occur mode, which also allows edits and
;; elpa-wgrep which allows edits in M-x grep buffers (basically).
;;
;; downside: doesn't support isearch, maybe consider phi-search as an
;; alternative (not in Debian either):
;; https://github.com/zk-phi/phi-search
(use-package multiple-cursors
  :defer t
  :ensure t
  :config
  (message "multiple-cursors not in Debian: http://bugs.debian.org/861127")
  :bind (("C-S-a" . mc/edit-lines)
	 ("C-S-s" . mc/mark-all-like-this)))

;; "snippets" are predefined templates that can be quickly entered in the current buffer
;;
;; yasnippet has a bunch of predefined ones and we have more in
;; ~/.emacs.d/snippets/, per mode. yasnippet also behaves as a
;; "autocomplete" system as you can insert a snippet by hitting "tab"
;; after a magic string.
;;
;; in Debian: elpa-yasnippet
(use-package yasnippet
  :defer 1
  :config
  (yas-global-mode 1))

;; show number of matches in modeline
;;
;; in Debian: elpa-anzu
(use-package anzu
  :defer 1
  :config
  (global-anzu-mode +1))

(autoload 'Lorem-ipsum-insert-list "lorem-ipsum")
(autoload 'Lorem-ipsum-insert-sentences "lorem-ipsum")
(autoload 'Lorem-ipsum-insert-paragraphs "lorem-ipsum")

;; highlight TODO items and such
;;
;; in Debian: elpa-hl-todo
(use-package hl-todo
  :defer 1
  :config
  (global-hl-todo-mode))

;; check buffers for syntax on the fly
;;
;; we don't use the builtin flymake because it's too limited:
;;
;; https://www.flycheck.org/en/latest/user/flycheck-versus-flymake.html
;;
;; that said, it seems like flymake has improved in Emacs 27, might be
;; worth taking a look again, see
;; https://www.masteringemacs.org/article/spotlight-flycheck-a-flymake-replacement
;;
;; in Debian: elpa-flycheck
(use-package flycheck
  :defer t
  ;; this is too dangerous, see
  ;; https://eshelyaron.com/posts/2024-11-27-emacs-aritrary-code-execution-and-how-to-avoid-it.html
  ;; https://security-tracker.debian.org/tracker/CVE-2024-53920
  ;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=37656
  ;;:hook (prog-mode . flycheck-mode)
  :config
  (use-package flycheck-grammalecte
    :ensure
    :config
    (message "flycheck-grammalecte not in Debian: https://bugs.debian.org/1020710")
    (flycheck-grammalecte-setup)))

(use-package flymake
  :custom (flymake-indicator-type 'fringes)
  :bind (("M-p" . 'flymake-goto-prev-error)
         ("M-n" . 'flymake-goto-next-error)))

;; trim trailing spaces at the end of lines
;;
;; in Debian: elpa-ws-butler
(use-package ws-butler
  :defer t
  :hook (prog-mode . ws-butler-mode))

;; format pretty much anything we have a binary for
;;
;; This is something LSP can do, but only if there's (a) a language
;; server and (b) if it supports it, which is far from universal. This
;; is a simpler plugin that has vastly more support for formatters out
;; of the box.
;;
;; in Debian: elpa-format-all
(use-package format-all
  :commands format-all-mode
  :hook (prog-mode . format-all-mode))

;; offline documentation browser
;;
;; This downloads manuals in ~/.emacs.d/devdocs and makes them
;; available offline.
;;
;; An alternative to this is Zeal, but zeal doesn't have
;; version-specific packages, e.g. it has "Python 3", not "Python
;; 3.9". Also the zeal integration in Emacs is limited: there's
;; zeal-at-point but that only starts a new Zeal program, not inside
;; emacs itself.
;;
;; Finally, devdocs takes extra care of formatting the document
;; unformly, while Zeal just shoves everyone's HTML in the bucket, so
;; all docs look different...
;;
;; There's also a commandline client, written in Rust:
;;
;; https://github.com/toiletbril/dedoc
;;
;; not in Debian
(use-package devdocs
  :commands (devdocs-lookup devdocs-peruse)
  :bind (("C-h D" . 'devdocs-lookup))
  :hook (python-mode-hook . (lambda () (setq-local devdocs-current-docs '("python~3.9")))))

;; chatgpt/LLM notes
;;
;; might be interesting to start using ChatGPT or some other LLM
;; (ideally, open!) as some sort of inline help system. basic
;; functionality would be chat/search, but autocompletion might be
;; interesting as well, at least be able to "send region to gpt" or
;; something.
;;
;; known options:
;;
;; ChatGPT.el https://github.com/joshcho/ChatGPT.el defers to
;; https://github.com/llm-workflow-engine/llm-workflow-engine (Python,
;; commandline, interesting on its own!).
;;
;; https://github.com/emacs-openai/chatgpt - part of a larger "openai"
;; package that also supports dall-e and so on
;;
;; features https://github.com/emacs-openai/eglot-uniteai as well,
;; which is a LSP frontend (eglot-based) for *another* python
;; interface for OpenAI, *specifically* designed as a LSP backend;;
;; https://github.com/freckletonj/uniteai
;;
;; also: https://github.com/SilasMarvin/lsp-ai
;;
;; there's also an lsp mode, but it's unclear why those are necessary when upstream has this:
;; https://github.com/freckletonj/uniteai/blob/master/clients/emacs/example_eglot_config.el
;; https://github.com/freckletonj/uniteai/blob/master/clients/emacs/example_lsp_mode_config.el
;;
;; also features https://github.com/emacs-openai/deepl
;;
;; https://khoj.dev/ is a whole other ball game, leveraging local data
;; and also allowing for local models, but it seems to pull you into
;; their cloud thing quite a bit
;;
;; see also https://notes.alexkehayias.com/using-chatgpt-with-emacs/
;; which basically reproduces the above list.
;;
;; for the commandline, GPT-4 suggested this
;; https://github.com/peterdemin/openai-cli it also suggests
;; https://github.com/openai/openai-python but that's just the
;; official Python API library.
;;
;; When asked about emacs packages, GPT-4 suggested this post
;; https://www.armindarvish.com/en/post/use_emacs_as_a_chatgpt_client/
;; which recommends:
;;
;; https://github.com/karthink/gptel
;;
;; which supports multiple backends, nice
;;
;; unfortunately, most of that stuff uses the OpenAI "API" which is
;; separate from "chatgpt", so you can't use it without adding credits
;; to your account separately. ugh.
;;
;; https://github.com/jart/emacs-copilot is an alternative using
;; github's shite
;;
;; Other options:
;; https://github.com/BerriAI/litellm
;; https://llm.datasette.io/
;; https://github.com/codeofdusk/gptcmd
;; https://github.com/aandrew-me/tgpt
;; https://ollama.com/ - local model runner, CLI and REST interfaces
;; https://salsa.debian.org/deeplearning-team/debgpt
;;
;; local modals to try:
;;
;; llama.cpp, debian ITP https://bugs.debian.org/1063673
;; whisper.cpp, Debian ITP https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1064101
;;
;; I asked chatgpt to calculate what would be cheaper between chatgpt
;; and the API. At 5 prompts per day, on average, the API is cheaper:
;;
;; API Cost Calculation with 5 Interactions per Day:
;;
;; Daily Cost:
;;
;;  * Your Input Tokens: Assuming one-sentence questions, let's
;;    estimate about 15 tokens per question. For 5 questions, this is
;;    5 * 15 tokens = 75 tokens.
;;
;;  * My Output Tokens: Estimating about 1,000 tokens per response,
;;    for 5 responses this is 5 * 1,000 tokens = 5,000 tokens.
;;
;;  * Total Daily Tokens: 75 (input) + 5,000 (output) = 5,075 tokens.
;;
;;  * Cost for Input Tokens: 75 tokens * $0.03/1,000 tokens = $0.00225.
;;
;;  * Cost for Output Tokens: 5,000 tokens * $0.06/1,000 tokens = $0.30.
;;
;;  * Total Daily Cost: $0.00225 (input) + $0.30 (output) ≈ $0.30225.
;;
;; Monthly Cost:
;;
;; Assuming 30 days in a month, the total monthly cost using the API
;; would be 30 days * $0.30225/day = $9.0675.
;;
;; Comparison:
;;
;;  * Chat Interface: $20/month.
;;  * API: $9.0675/month.
;;
;; update: gptel seems like the more solid option, after reviewing the
;; reviews above. ChatGPT.el is ... interesting because it delegates
;; to a Python backend that we *could* also use directly, but I
;; actually looked at that backend and prefered the llm.datasette.io
;; thing instead, which I have packaged in Debian, even
;; (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065572).
;;
;; I have also switched to the API thing and canceled my GPT+
;; subscription. It seems like API money *expires* after a while
;; though which is really unfortunate...
;;
;; not packaged in Debian
(use-package gptel
  :ensure t
  :defer t
  :commands (gptel-send gptel gptel-menu)
  :custom
  (gptel-model "gpt-4"))

;; collaborative editing
;;
;; wouldn't that be nice? unfortunately, we can't quite have nice things.
;;
;; here are all the non-working options:
;;
;; https://www.emacswiki.org/emacs/Rudel made it work with old gobby
;; once, broken with new infinoted. supposedly supports SubEthaEdit
;; but that's been dead for years.
;;
;; https://codeberg.org/zzkt/ethermacs etherpad client. talks with the
;; API, which means it overwrites the entire file. preliminary work
;; for collaborative editor present, minimal MVP, untested. inactive
;; for 3 years as of 2024-01-24.
;;
;; https://code.librehq.com/qhong/crdt.el actually works as a
;; standalone CRTD collaborative editor (!). supposedly supports TLS,
;; (through stunnel?). shows multiple cursors and seem stable. can
;; follow users around. authorship colors. amazing stuff. suggests
;; IPv6, SSH tunnels, or https://gitlab.com/gjedeer/tuntox for global
;; reachability. could use a onion service support.
;;
;; https://github.com/zk-phi/togetherly/ is an another minor mode to
;; edit files collaboratively, client/server, unclear what the
;; protocol is.
;;
;; https://www.codetogether.com/live - non-free, but with plugins for
;; IntelliJ, Eclipse, VS Code... VS Code has its own collaborative
;; thing as well, and there are other plugins for VS code:
;; https://visualstudio.microsoft.com/services/live-share/
;; https://github.com/PeerCodeProject/PeerCode
;;
;; https://github.com/typeintandem/tandem - is another thing that made
;; the new 6 years ago, but has completely stalled then, and didn't
;; have an emacs plugin either
;; https://github.com/typeintandem/tandem/issues/125
;;
;; see also https://www.emacswiki.org/emacs/CollaborativeEditing
;;
;; tested this:
;;
;;(use-package etherpad
;;  :ensure t
;;  :config (setq etherpad-server "https://riseup.net"
;;		etherpad-apikey ""
;;		etherpad-autosync nil)
;;  :bind (("C-c e" . etherpad-edit)
;;	 :map etherpad-mode-map
;;	 ("C-c c" . etherpad-save)))
;;
;; but missing an api key, and the API endpoint gives a 404


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PROGRAMMING MODES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook 'c-mode-common-hook
 	  (lambda ()
 	    (auto-fill-mode 1)
	    (set (make-local-variable 'fill-nobreak-predicate)
		 (lambda ()
		   (not (eq (get-text-property (point) 'face)
			    'font-lock-comment-face))))))

(with-eval-after-load "python"
  ;; duh - why isn't this the default (PEP8)
  (setq-default py-indent-offset 4))

;; not in Debian: https://github.com/tkf/emacs-ipython-notebook/issues/199
(defun anarcat/ein:restart-execute-all (ws)
  "Restart the kernel, wait for ready, then execute all cells.

WS is the worksheet to operate on, and defaults to the current one."
  (interactive (list (ein:worksheet--get-ws-or-error)))
  (ein:notebook-restart-kernel-command)
  ;; wait for the kernel to *stop*
  (sit-for 1.0) ;; we should not need this.
  (let ((kernel (ein:get-kernel--worksheet))
        (timeout 3.0) ;; XXX: should be a parameter
        (tick-time 0.1))
    (loop repeat (floor (/ timeout tick-time))
          do (sit-for tick-time)
          until (ein:kernel-live-p kernel))
    (ein:worksheet-execute-all-cell ws)))

;; in Debian: elpa-puppet-mode
(use-package puppet-mode
  :defer t
  :mode ("\\.pp" . puppet-mode))

;; prefer KNF styles
(eval-after-load "cc-styles"
  '(progn
     (load "knf.el" t)
     ))

;; default go mode,mor or less
;; other stuff we could add:
;; oracle mode and goimports:
;; http://tleyden.github.io/blog/2014/05/27/configure-emacs-as-a-go-editor-from-scratch-part-2/
;; goflymake: https://github.com/dougm/goflymake
;; errcheck: https://github.com/dominikh/go-errcheck.el
;; goeldoc: https://github.com/syohex/emacs-go-eldoc
;; quickhelp? https://github.com/expez/company-quickhelp
;;
;; otherwise we use the following Debian packages:
;; gocode
;; golang
;; golang-mode
;; golint
;; godef
(use-package go-mode
  :defer t
  :config
  (add-hook 'go-mode-hook
            (lambda ()
              (if (not (string-match "go" compile-command))
                  (set (make-local-variable 'compile-command)
                       "go build -v && go test -v && go vet && golint && gofmt -s -l ."))
              ;; fmt files on save
              (add-hook 'before-save-hook 'gofmt-before-save)
              ;; reduce tab width, since everything is tabbed in go and it's too wide
              (setq-default tab-width 4
                            standard-indent 4))))

;; in Debian
(use-package dockerfile-mode
  :defer t
  :mode "Dockerfile.*\\'")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VERSION CONTROL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; I'm not using magit for all git interactions in Emacs
;;
;; since i'm not using other version control systems anymore, that
;; basically makes the built-in "vc" package obsolete.
;;
;; in Debian: elpa-magit
(use-package magit
  :defer t
  :init
  ;; to fix the error "if: Key sequence C-x M-g starts with non-prefix
  ;; key C-x ESC". this is still a problem in Emacs 29
  (setq magit-define-global-key-bindings nil)
  :bind (("C-x g" . magit-status)
         ("C-x C-g" . magit-status)
         ([f7] . magit-status)
         ([f6] . magit-file-dispatch))
  :custom
  ;; show word diff inside *all* hunks, not just current
  (magit-diff-refine-hunk 'all)
  ;; https://magit.vc/manual/magit/Wip-Modes.html
  (magit-wip-mode t)
  ;; make the revisions look more like "git show". the default is
  ;; somewhat similar to --format=fuller, this is similar to the
  ;; "medium" format, except I can't figure out how to add the
  ;; "commit" prefix on the first line
  (magit-revision-headers-format "Author:     %aN <%aE>
Date: %ai
")
  :config
  ;; this removes the refs pasted before the diff, we keep it because
  ;; it's really useful and can be removed when copy-pasting.
  ;; (setq magit-revision-insert-related-refs t)

  ;; cargo-culted from https://github.com/magit/magit/issues/3717#issuecomment-734798341
  ;; valid gitlab options are defined in https://docs.gitlab.com/ee/user/project/push_options.html
  ;;
  ;; the second argument to transient-append-suffix is where to append
  ;; to, "-n" is "--dry-run", the last argument before we start modifying this
  (transient-append-suffix 'magit-push "-n"
    '(1 "=s" "Skip GitLab CI" "--push-option=ci.skip"))
  (transient-append-suffix 'magit-push "=s"
    '(1 "=m" "Automatically create MR" "--push-option=merge_request.create"))
  (transient-append-suffix 'magit-push "=m"
    '(1 "=d" "Set as draft" "--push-option=merge_request.draft"))
  (transient-append-suffix 'magit-push "=d"
    '(1 "=a" "Automatic merge" "--push-option=merge_request.merge_when_pipeline_succeeds"))
  (transient-append-suffix 'magit-push "=a"
    '(1 "=r" "Remove the source branch" "--push-option=merge_request.remove_source_branch"))
  (transient-append-suffix 'magit-push "=r"
    '(1 "=o" "Set push option" "--push-option="))
  (transient-append-suffix 'magit-push "=o"
    '(1 "=V" "Set CI variable" "--push-option=ci.variable="))  ;; Will prompt, can only set one extra variable
  ;; we used to do this:
  ;; :hook
  ;;(magit-status-sections . magit-insert-modules)
  ;; but it was *extremely* slow, now just do `magit-list-submodules` instead.
  )

;; show TODO items in the magic buffers
;;
;; in Debian: elpa-magit-todos
;;
;; crashes with this on magit load:
;; custom-initialize-reset: Cannot open load file: No such file or directory, ob-ledger
;;(use-package magit-todos
;;  :commands (magit-todos-mode)
;;  :hook (magit-mode . magit-todos-mode))

;; git tricks from the commandline missing from magit:
;; git dw = diff --color-words

;; other replacements:
;;
;; git lg = log --graph --pretty=format:'%Cred%h%Creset%C(yellow)%d%Creset %s %Cgreen(%ar) %C(blue)<%an>%Creset' --abbrev-commit --date=relative
;; this is simply "magit-log" which is pretty powerful on its own
;;
;; git s = status --short --branch
;; all well shown in magit-status
;;
;; git pullr = pull --rebase
;; the "r" key in "F" (pull) will toggle `branch.master.rebase' which
;; isn't ideal because that's sticky, but it should work

;; vc replacements in magit:
;;
;; `vc-annotate' (`C-x C-v g`): `magit-blame'
;; `vc-diff' (`C-x C-v ='): `magit-diff-buffer-file'
;; `vc-git-grep': no replacement
;; `vc-next-action' (`C-x C-q'): `anarcat/magic-commit-buffer', see below
(defun anarcat/magit-commit-buffer ()
  "Commit diff in the current buffer on the fly.

This is different than `magit-commit' because it calls `git
commit' without going through the staging area AKA index
first.  This is a replacement for `vc-next-action'.

Tip: setting the git configuration parameter `commit.verbose' to
2 will show the diff in the changelog buffer for review.  See
`git-config(1)' for more information.

An alternative implementation was attempted with `magit-commit':

  (let ((magit-commit-ask-to-stage nil))
    (magit-commit (list \"commit\" \"--\"
                        (file-relative-name buffer-file-name)))))

But it seems `magit-commit' asserts that we want to stage content
and will fail with: `(user-error \"Nothing staged\")'. This is
why this function calls `magit-run-git-with-editor' directly
instead."
  (interactive)
  (magit-run-git-with-editor (list "commit" "--" (file-relative-name buffer-file-name))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TEXT MODES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(autoload 'moinmoin-mode "moinmoin")

;; see https://github.com/jrblevin/markdown-mode/pull/714
(defun markdown-search-file-with-extension (file)
  "Search for a file named FILE but also common markdown extensions."
  (if (file-exists-p file)
      file
    (cl-loop for extension in '("mdown" "mkd" "md" "markdown")
             for md-file = (concat file "." extension)
             when (file-exists-p md-file)
             return md-file
             finally return file)))

;; in Debian: elpa-markdown-mode
;; workaround for Debian bug #1036359
(setq native-comp-deferred-compilation-deny-list '("markdown"))

(use-package markdown-mode
  :defer t
  :mode "\\.mdwn"
  :custom
  (markdown-command "multimarkdown")
  (markdown-footnote-location 'header)
  (markdown-reference-location 'header)
  ;; [[text|path]] supports
  (markdown-enable-wiki-links t)
  (markdown-wiki-link-search-parent-directories t)
  (markdown-translate-filename-function 'markdown-search-file-with-extension)
  ;; fontify code blocks
  (markdown-fontify-code-blocks-natively t)
  :config
  (define-key markdown-mode-map (kbd "M-p") 'markdown-previous-link t)
  (define-key markdown-mode-map (kbd "M-n") 'markdown-next-link t)
  ;; in Debian: elpa-markdown-toc
  ;;
  ;; alternative: md_toc -l github -o . < file.md
  ;; from python3-md-toc
  (use-package markdown-toc)
  :hook
  (markdown-mode . eglot-ensure)
  (markdown-mode . auto-fill-mode))
;; TODO: markdownfmt:
;; https://github.com/shurcooL/markdownfmt
;; (use-package markdownfmt
;;  :config
;;  (define-key markdown-mode-map (kbd "C-c C-f") #'markdownfmt-format-buffer)
;;  (add-hook 'markdown-mode-hook #'markdownfmt-enable-on-save))
;; (use-package poly-markdown)
;; https://github.com/polymode/poly-markdown

(defun anarcat/markdown-convert-link ()
  "Convert markdown inline links to references.

This convers a [text](link) link format into [text][] ... [text]:
linké

Allowing search-backwards (say with a prefix) could also be
interesting.

This is discussed in:

https://github.com/jrblevin/markdown-mode/issues/94

Should eventually merged upstream once it is acceptable elisp."
  (interactive nil markdown-mode)
  ;; find unconverted link
  ;; we have 3 groups in here:
  ;; 1. text (without brakets)
  ;; 2. link (with parens, to replace it with replace-match)
  ;; 3. link (without parens, to extract it to insert it later)
  (if (re-search-forward "\\[\\([^]]*\\)]\\((\\([^)]*\\))\\)" nil t)
      (let ((text (match-string 1))
            (link (match-string 3)))
        ;; remove the link and replace with brakets
        ;; link is the "2" below, and replace only that
        (replace-match "[]" nil nil nil 2)
        ;; go at the end of the paragraph and add stuff there
        (save-excursion
          (search-forward "\n\n" nil 1)
          ;; insert the link reference
          (insert (format " [%s]: %s\n\n" text link))
          ;; XXX: we insert two newlines to avoid breaking the paragraph
          ;; break, but that makes too many spaces if we insert many links
          ;; like this. not sure how to fix this.
          ))
    (message "no inline link found until end of buffer")))

(fset 'anarcat/md-link-cve
      [?\C-s ?C ?V ?E left C-left ?\[ ?\C-  C-right C-right C-right ?\M-w ?\] ?\( ?\C-x ?r ?g ?a ?\C-x ?\C-x ?\C-y ?\) left ?\C-c ?\C-a ?r return])

(defun anarcat/make-slug (beg end)
  "Turn a string into a slug.

The `BEG' and `END' arguments delimit the region to apply the slug on.

Inspired by https://pages.sachachua.com/.emacs.d/Sacha.html"
  (interactive "r")
  (downcase-region beg end)
  (replace-regexp "[^a-z0-9]+" "-" nil beg end)
  (replace-regexp "^-\\|-$" "" nil beg end))

;; add more file types
(add-to-list 'auto-mode-alist '("^README$" . text-mode))
(add-to-list 'auto-mode-alist '("^\\.db$" . zone-mode))
(add-to-list 'auto-mode-alist '("^\\.\\(?:org\\|net\\)$" . zone-mode)) ;; Tor's zonefile are like that
(add-to-list 'auto-mode-alist '("\\.rules\\'" . yaml-mode)) ;; Prometheus alerting and recording rules

;; in Debian: elpa-ledger
(use-package ledger
  :defer t
  :mode ("\\.lgr" . ledger-mode)
  :config (setq ledger-default-date-format "%Y-%m-%d"))

(require 'wc nil t)

(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; should prompt
(defun anarcat/change-dict (d)
  "Prompt user for new dictionnary (D) and flyspell."
  (interactive "Mdict: ")
  (ispell-change-dictionary d)
  (flyspell-buffer)
  (flyspell-mode 1)
  )

;; automatically change dictionnary language based on content
;;
;; in Debian: elpa-auto-dictionary
(use-package auto-dictionary
  :defer t
  :commands auto-dictionary-mode
  :hook (flyspell-mode . auto-dictionary-mode))

;; wc-mode shows the number of words in the mode line along with the
;; number of added or removed words since the buffer was opened
;;
;; in Debian: elpa-wc-mode
(use-package wc-mode
  :defer 1)

(defun anarcat/author-mode ()
  "Toggle a bunch of minor modes for authorship.

This should be converted to a minor mode in itself, or maybe as
hooks from writeroom?

See Elisp manual, 22.3 Minor Modes."
  (interactive)
  (writegood-mode)
  (wc-mode)
  (writeroom-mode)
  (adict-guess-dictionary)
  (message "Author-mode setup toggled"))

;; in Debian: elpa-writegood-mode
(use-package writegood-mode
  :defer 1)

;; we don't use olivetti mode (which *is* packaged in Debian) because
;; it doesn't support the "global effects" which, in writeroom,
;; disables the modeline, sets the window to be sticky and so
;; on... there is a "on-hook" but no "off hook" (!?). the
;; visual-fill-column-mode that writeroom-mode uses basically does the
;; same as olivetti anyways
;;
;; see also: https://stackoverflow.com/a/71028088
;;
;; related: https://github.com/KeyWeeUsr/typewriter-roll-mode
(use-package writeroom-mode
  :defer t
  :custom
  ;; original is: (writeroom-set-fullscreen writeroom-set-alpha writeroom-set-menu-bar-lines writeroom-set-tool-bar-lines writeroom-set-vertical-scroll-bars writeroom-set-bottom-divider-width)
  (writeroom-global-effects
   '(writeroom-set-alpha writeroom-set-menu-bar-lines writeroom-set-tool-bar-lines writeroom-set-vertical-scroll-bars writeroom-set-sticky sway-toggle-floating sway-toggle-fullscreen))
  ;; still show the modeline
  (writeroom-mode-line t)
  ;; try to restore window configuration after
  (writeroom-restore-window-config t)
  :config
  (defun wm-toggle-fullscreen (_)
    (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                           '(2 "_NET_WM_STATE_FULLSCREEN" 0)))
  (defun xmonad-toggle-sticky (_)
    ;; this requires a special piece of code on the xmonad side to
    ;; handle that event and do a copyToAllToggle
    (x-send-client-message nil 0 nil "XMONAD_COPY_ALL_SELF" 8 '(0)))
  (defun i3-toggle-floating (_) (start-process "i3-msg" nil "i3-msg" "[con_id=__focused__]" "floating" "toggle"))
  (defun sway-toggle-floating (_) (start-process "swaymsg" nil "swaymsg" "[con_id=__focused__]" "floating" "toggle"))
  (defun sway-toggle-fullscreen (_) (start-process "swaymsg" nil "swaymsg" "[con_id=__focused__]" "fullscreen" "toggle"))
  )

; in Debian: elpa-visual-fill-column-mode
(use-package visual-fill-column-mode
  :defer t
  :custom
  (visual-fill-column-center-text t)
  (visual-fill-column-extra-text-width '(10 . 30)))

;; I don't use org mode anymore, but when I did it was for time
;; tracking
;;
;; in Debian: org-mode
(use-package org
  :defer t
  ;; from https://changelog.complete.org/archives/9861-emacs-1-ditching-a-bunch-of-stuff-and-moving-to-emacs-and-org-mode
  ;;:bind (("C-c l" . org-store-link)
  ;;       ("C-c a" . org-agenda)
  ;;       ("C-c c" . org-capture)
  ;;       ("C-c b" . org-iswitchb))
  :custom
  (org-agenda-files '("~/finances/"))
  (org-babel-load-languages '((emacs-lisp . t)))
  (org-clock-clocked-in-display 'both)
  (org-clock-history-length 20)
  (org-clock-idle-time 10)
  (org-clock-into-drawer nil)
  (org-clock-mode-line-total 'today)
  (org-clock-out-remove-zero-time-clocks t)
  (org-clock-out-when-done nil)
  (org-clock-persist t)
  (org-clock-persist-query-save nil)
  (org-clock-report-include-clocking-task t)
  (org-clock-x11idle-program-name "xprintidle")
  (org-duration-format 'h:mm)
  (org-support-shift-select 'always)
  (org-time-clocksum-format
   '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))

  :config
  (add-to-list 'load-path "/usr/share/org-mode/lisp")
  (require 'ox-md nil t)
  (if (require 'org-clock nil t)
      (progn
        ;; setup hooks so that clock and history is saved, see
        ;; customized org-clock-persist variable
        (org-clock-persistence-insinuate)
        ;; make clock in/out shortcuts global. org-clock-in can't be global, it seems.
        (global-set-key (kbd "C-c C-x C-o") 'org-clock-out)
        (global-set-key (kbd "C-c C-x C-x") 'org-clock-in-last))))

;; workaround broken indent function in org-mode 8.2
(defun org-clocktable-indent-string-83 (level)
  "Return indentation string according to LEVEL.
LEVEL is an integer.  Indent by two spaces per level above 1."
  (if (= level 1) ""
    (concat (make-string (* (- level 2) 1) ?*) "* " )))
(advice-add 'org-clocktable-indent-string :override #'org-clocktable-indent-string-83)

;; in Debian: elpa-pdf-tools
;;
;; alternative: https://github.com/dalanicolai/doc-tools with support
;; for mupdf, multipage and continuous displays
(use-package pdf-tools
  :defer t
  :config (pdf-tools-install))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAIL CONFIGURATION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; I read all my mail in notmuch-emacs
;;
;; in Debian: elpa-notmuch
(use-package notmuch
  :defer t
  :config
  (use-package notmuch-addr
    :ensure t)
  (with-eval-after-load 'notmuch-address
    (notmuch-addr-setup))
  :hook (notmuch-show-mode . visual-fill-column-mode)
  :custom
  (notmuch-address-command "notmuch-address")
  (notmuch-always-prompt-for-sender t)
  (notmuch-archive-tags '("-inbox" "-unread" "-flagged"))
  (notmuch-crypto-process-mime t)
  (notmuch-draft-tags '("+draft" "-inbox"))
  (notmuch-fcc-dirs '((".*" . ".Sent -inbox -flagged -unread +sent")))
  (notmuch-message-headers
   '("Date" "From" "To" "Cc" "Bcc" "Reply-To" "Subject" "Archived-At" "Archive-At" "List-Unsubscribe"))
  (notmuch-print-mechanism 'notmuch-print-muttprint/evince)
  (notmuch-search-line-faces '(("deleted" :foreground "red") ("unread" :weight bold)))
  (notmuch-search-oldest-first nil)
  (notmuch-show-all-multipart/alternative-parts nil)
  (notmuch-show-all-tags-list t)
  (notmuch-show-insert-text/plain-hook
   '(notmuch-wash-convert-inline-patch-to-part notmuch-wash-tidy-citations notmuch-wash-elide-blank-lines notmuch-wash-excerpt-citations))
  (notmuch-tag-formats '(("unread" (propertize tag 'face 'notmuch-tag-unread))))
  (notmuch-tagging-keys
   '(("a" notmuch-archive-tags "Archive")
     ("u" notmuch-show-mark-read-tags "Mark read")
     ("f" ("+feathers") "Feathers")
     ("s" ("+spam" "-inbox" "-flagged" "-unread" "-ham") "Mark as spam")
     ("h" ("-spam" "-greyspam" "+inbox" "+flagged" "+ham") "Mark as ham")
     ("t" ("+todo") "Mark as todo")
     ("d" ("+deleted" "-inbox") "Delete"))))

;; set identity based on From address or other settings
(use-package gnus-alias
  :defer 1
  :ensure t
  :config (message "gnus-alias not in Debian: not reported"))

;; mutt email edition support
(add-to-list 'auto-mode-alist '("/\\(?:neo\\)?mutt-" . mail-mode))
;; make mail mode auto-fill, abbrev
(add-hook 'mail-mode-hook
          (lambda ()
            (auto-fill-mode 1)
            (flyspell-mode 1)
            (abbrev-mode 1)))
(add-hook 'message-mode-hook
          (lambda ()
            (auto-fill-mode 1)
            (flyspell-mode 1)))

(add-hook 'message-signature-setup-hook 'fortune-to-signature)

;; most SMTP servers expect a FQDN on HELO and (system-name) returns
;; only the short hostname since Emacs25, fallback to hostname -f
(when (>= emacs-major-version 25)
  (setq smtpmail-local-domain (car (split-string (shell-command-to-string "hostname -f")))))

;; to import pgp keys:
;; application/pgp-keys; gpg --import --interactive --verbose;needsterminal

;; to make have different setups for different accounts...
;; http://www.emacswiki.org/cgi-bin/wiki/GnusMSMTP#toc2


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COLOR AND DISPLAY SETTINGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; rainbow-mode shows hex colors visually by changing the background color
;;
;; in Debian: elpa-rainbow-mode
(use-package rainbow-mode
  :defer 1)

;; ignore Xresources file
;;
;; the reason we do this is that the X resources specify things like
;; background and so on, which is useful for setting colors in a
;; theme. but here we *also* manually (and dynamically!) change the
;; theme so we do *not* want things like the background to be manually
;; set, otherwise there's no way to return to the "default" Emacs
;; colors that we could get by *disabling* our custom theme.
;;
;; another way to do this might be:
;; (set-face-attribute 'default nil :family "Consolas" :height 150) ; change font
;; see https://readingworldmagazine.com/emacs/2021-09-22-how-to-configure-emacs-themes/
(setq inhibit-x-resources t)
;; this means we also need to set the font, which duplicates what we
;; have in Xresources for other things.
(add-to-list 'default-frame-alist '(font . "Commit Mono"))

(when (>= emacs-major-version 25)
  ;; resize in pixels, not chars
  ;;
  ;; we *don't* enable pixel-scroll-mode (introduced in Emacs 26) as
  ;; it feels jerky
  (setq frame-resize-pixelwise t))

;; use the srcery theme
(use-package srcery-theme
  :ensure
  :config
  (message "srcery-theme not in Debian: not reported")
  (load-theme 'srcery t))

(defun anarcat/theme-toggle ()
  "Toggle between light and dark themes."
  (interactive)
  (if (custom-theme-enabled-p 'srcery)
      (anarcat/theme-light)
    (anarcat/theme-dark)))

(defun anarcat/theme-dark ()
  "Switch to a dark theme."
  (interactive)
  (setq frame-background-mode 'dark)
  (mapc 'frame-set-background-mode (frame-list))
  (disable-theme 'dichromacy)
  (load-theme 'srcery t))

(defun anarcat/theme-light ()
  "Switch to a light theme."
  (interactive)
  (setq frame-background-mode 'light)
  (mapc 'frame-set-background-mode (frame-list))
  (disable-theme 'srcery)
  (load-theme 'dichromacy t))

;; colorize compilation logs properly
(when (require 'ansi-color nil t)
  (defun display-ansi-colors ()
    "process current buffer to parse escape sequences properly

WARNING: this will ignore read-only settings so saving the buffer
might modify the backing file.

Cargo-culted from https://stackoverflow.com/a/23382008/1174784"
    (interactive)
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region (point-min) (point-max))))

  ;; cargo-culted from https://emacs.stackexchange.com/a/38531/11929
  ;; and http://endlessparentheses.com/ansi-colors-in-the-compilation-buffer-output.html
  (defun endless/colorize-compilation ()
    "Colorize from `compilation-filter-start' to `point'."
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region
       compilation-filter-start (point))))

  ;; This should normally done by comint-mode but switching to
  ;; comint-mode after compilation has started somewhat does not
  ;; trigger the proper filters. So we call add a hook which will
  ;; colorize each line correctly.
  (add-hook 'compilation-filter-hook
            #'endless/colorize-compilation))

;; dim unfocused windows
;;
;; in Debian: elpa-dimmer
(use-package dimmer
  :defer 1
  :diminish
  :config
  ;; fails with a weird error
  ;;(dimmer-configure-company-box)
  ;; not in Debian buster
  ;;(dimmer-configure-magit)
  (dimmer-configure-which-key)
  (dimmer-mode t))

;; modeline tweaking: color-coding, elision, prefixes.
;;
;; in Debian: elpa-smart-mode-line
;;
;; actually not that useful - the filename width is larger than the
;; default, which actually *hides* a bunch of modes in my tests. meh.
(use-package smart-mode-line
  :defer 1
  :ensure
  :init
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'respectful)
  :config
  (sml/setup)
  ;; default is 44, and a bit too wide, this sets 10-25 as min-max
  (setq sml/name-width '(10 . 25)))

;; workaround failure of emacs --daemon to load custom-set-faces properly
;; see https://emacs.stackexchange.com/questions/46541/running-emacs-as-a-daemon-does-not-load-custom-set-faces
(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (when (eq (length (frame-list)) 2)
                  (progn
                    (select-frame frame)
                    (custom-set-faces '(cursor ((t (:background "#990A1B"))))))))))

;; ???
(add-hook 'write-file-functions 'highlight-changes-rotate-faces nil t)

;; use only "frames" (which are really "windows") instead of "windows"
;; (which are really "panes" but Emacs was created before Windows were
;; invented).
;;
;; we prefer this because we use a tiling window manager that arranges
;; windows like this already. it's a *little* inconvenient in some
;; edge cases because Emacs does make an extra effort of correctly
;; arranging windows for us often, but for the general case it means
;; we don't need to switch mental models between the window manager
;; and Emacs when trying to move stuff around (and it *is* much easier
;; to move stuff around in the window manager than within emacs).
(use-package frames-only-mode
  :ensure
  :defer 1
  :init
  (frames-only-mode)
  ;; note that this is the default, in menu-bar.el. when the menu bar
  ;; is disabled (like here) it pops open a submenu instead, which is
  ;; fine for us. also note the C-[f10] keybinding which gives us a
  ;; nice menu listin.
  ;; (global-set-key [f10] 'menu-bar-open)
  :custom
  ;; disable menu bar now that we (over)use frames, as it makes
  ;; everything prettier and more lightweight
  (menu-bar-mode nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BEHAVIOR SETTINGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; text typed replaces current selection
(delete-selection-mode)
;; ???
(setq minibuffer-max-depth nil)
;; enable narrow functions
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
;; stop forcing me to spell out "yes"
(fset 'yes-or-no-p 'y-or-n-p)
;; disable the tool bar
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

;; use UTF-8 everywhere
(set-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8-unix)
(prefer-coding-system 'utf-8-unix)
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))
(setq-default buffer-file-coding-system 'utf-8-unix)

;; use dir/foo.el instead of foo.el<dir> for duplicate buffer names
(require 'uniquify)
(setq uniquify-separator "/"               ;; The separator in buffer names.
      uniquify-buffer-name-style 'forward) ;; names/in/this/style


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; KEYBINDINGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: consider god-mode? https://github.com/emacsorphanage/god-mode

;; make the region act quite like the text "highlight" in many apps.
;; (transient-mark-mode 1)  ; Now on by default
;; allow shifted cursor-keys to control the region.
;; (setq shift-select-mode t) ; Now on by default

;; see also https://shallowsky.com/blog/linux/x-selection-keys.html
;; stop mouse selection from being immediately injected to the kill ring
(setq mouse-drag-copy-region nil)
;; stop killing/yanking interacting with primary X11 selection
(setq select-enable-primary nil)
;; make killing/yanking interact with clipboard X11 selection
(setq select-enable-clipboard t)

(defun yank-primary-selection ()
  "Insert the PRIMARY selection from the windowing system."
  (interactive)
  (insert
   (gui-get-primary-selection)))
;; control-v still yank from CLIPBOARD, we reserve shift-insert and
;; control-y for PRIMARY, which matches the rxvt behavior (although it
;; uses control-shift-v for clipboard)
(global-set-key [(control y)] 'yank-primary-selection)
(global-set-key [(shift insert)] 'yank-primary-selection)

;; these will probably be already set to these values, leave them that way if so!
;; (setf interprogram-cut-function 'x-select-text)
;; (setf interprogram-paste-function 'x-cut-buffer-or-selection-value)
;; You need an emacs with bug #902 fixed for this to work properly. It has now been fixed in CVS HEAD.
;; it makes "highlight/middlebutton" style (X11 primary selection based) copy-paste work as expected
;; if you're used to other modern apps (that is to say, the mere act of highlighting doesn't
;; overwrite the clipboard or alter the kill ring, but you can paste in merely highlighted
;; text with the mouse if you want to)
(setq select-active-regions t) ;  active region sets primary X11 selection
(global-set-key [mouse-2] 'mouse-yank-primary)  ; make mouse middle-click only paste from primary X11 selection, not clipboard and kill ring.
;; with this, doing an M-y will also affect the X11 clipboard, making emacs act as a sort of clipboard history, at
;; least of text you've pasted into it in the first place.
;; (setq yank-pop-change-selection t)  ; makes rotating the kill ring change the X11 clipboard.

;; set mouse-3 to a useful context menu instead of the mind-boggling
;; `mouse-save-then-kill'
(when (>= emacs-major-version 28)
  (context-menu-mode 1))

;; "suspend emacs" usually makes absolutely no sense, unless we run in
;; a terminal, which we basically never do. when we run in a GUI, it's
;; supposed to "iconify" or "minimize", but in my tiling WM, it makes
;; no sense either.
;;(global-unset-key [(control z)])

;; typical first-row shortcuts... this is inspired from:
;; https://wiki.systemcrafters.cc/emacs/
(global-set-key [(control z)] 'undo)
(global-set-key [(control shift z)] 'redo)
;; control-x: prefix-command
(global-set-key [(control shift x)] 'clipboard-kill-region)
;; control-c: prefix-command
(global-set-key [(control shift c)] 'clipboard-kill-ring-save)
;; control-v is 'scroll-up-command, which I didn't know until I
;; tried. map it to the more usual `paste`
(global-set-key [(control v)] 'clipboard-yank)
;;(global-set-key [(control v)] 'yank)
;; cua messes with M-y, and anyways it's weird to do M-y now
(global-set-key [(meta v)] 'yank-pop)
(global-set-key [(control a)] 'mark-whole-buffer)
;; remaining keymappings that conflict with typical ones:
;; control-w: kill-region, would normally be control-x, but is
;; typically "close window" (AKA 'delete-frame) in other programs. too hard to get rid of
;; meta-y: kill-ring-save, would normally be control-c
;;
;; i was previously using cua-mode for this, but found it too
;; disruptive

;; create keybindings to zoom text.
;;
;; this is bound to C-x C-=, C-x C-- by default and behaves better as
;; it is "sticky": hitting only +/- keeps on setting the thing.
(global-set-key (kbd "C-M-+") 'text-scale-adjust)
;; same without shift
(global-set-key (kbd "C-M-=") 'text-scale-adjust)
(global-set-key (kbd "C-M--") 'text-scale-adjust)
(global-set-key (kbd "C-M-0") 'text-scale-adjust)

;; remove annoying warning when i hit control-pgX by mistake
(put 'scroll-left 'disabled nil)

(defun next-page-narrow ()
  "Move to next page and `narrow-to-page'."
  (interactive)
  (narrow-to-page 1))
(defun previous-page-narrow ()
  "Move to previous page and `narrow-to-page'."
  (interactive)
  (narrow-to-page -1)
  (beginning-of-buffer))
(global-set-key [(control x) (control \[)] 'previous-page-narrow)
(global-set-key [(control x) (control \])] 'next-page-narrow)

;; Make it easier to discover key shortcuts
;;
;; possible alternative? https://github.com/minad/marginalia
;;
;; in debian: elpa-which-key
(use-package which-key
  :defer 1
  :diminish
  :config
  (which-key-mode)
  (which-key-setup-side-window-bottom)
  (setq which-key-idle-delay 1.0))

;; our custom functions keybindings
(global-set-key [(control tab)] 'other-window)

;; comform to https://en.wikipedia.org/wiki/ISO/IEC_14755
;; specifically, this is GNOME's keybinding:
;; https://en.wikipedia.org/wiki/Unicode_input#In_X11_(Linux_and_Unix_variants)
(global-set-key [(control shift u)] 'insert-char)

;; f-keys mapping
;;
;; builtins:
;; [f1] help
;; [f2] two-column.el (!?) elsewhere: (e.g. file managers) rename
;; [f3] kmacro-start-macro-or-insert-counter
;; [f4] kmacro-end-or-call-macro
;; devel keymaps map
(global-set-key [f5] 'font-lock-fontify-buffer)
;; defined elsewhere
;; [f6] magit-file-dispatch (e.g. commit, blame, etc)
;; [f7] magit-status
;; [f8] rg-project, previously rgrep
(global-set-key [f9] 'compile)
(global-set-key [(control f9)] 'recompile)
(global-set-key [f12] 'gdb)

;; builtin:
;; [f11] devhelp-word-at-point (Debian thing?)
;; [f12] unused!
;; [f12] used to be 'call-last-kbd-macro but that's actually builtin
;; on [f4] (and better, because it doubles as a kmacro-end-macro
;; replacement as well, so i just need [f3] [f4] now.

(global-set-key [(meta backspace)] 'backward-kill-word)
(global-set-key [(meta delete)] 'kill-word)

(global-set-key [(meta left)] 'backward-sexp)
(global-set-key [(meta right)] 'forward-sexp)

(when (< emacs-major-version 29)
  ;; added by spwhitton, presumably part of 29 eventually:
  ;; https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=ad89ec84ee20a027e36922c187ad9f2dcb93bcaf
  ;; M-g i: jump to imenu entry
  (define-key goto-map    "i" 'imenu))
;; related, but not in emacs yet.
(define-key goto-map    "I" 'imenu-add-menubar-index)

;; scroll-lock normally invokes scroll-lock-mode, which I just learned
;; about *today* and confuses the hell out of me every *other* day. so
;; just disable that thing. in "view-mode", I think it will get
;; enabled anyway, which is fine, but I can't think of any situation
;; where I want scroll lock to do its thing here.
;;
;; and besides, that keybinding doesn't even work: it's only in some
;; weird corner cases that it seems to be active, and then I can't
;; turn the damn thing off.
(global-set-key [scroll-lock] 'ignore)

;; open a file right away on enter in pcl-cvs
;;(define-key 'cvs-mode-map [(return)] 'cvs-mode-find-file)

(use-package crux
  :defer 1
  :ensure t
  :config (message "crux not in Debian: http://bugs.debian.org/909337"))
;; not in debian
(use-package shift-number
  :defer t
  :bind (("C-c a" . shift-number-up)
         ("C-c x" . shift-number-down))
  :ensure t
  :config (message "shift-number not in Debian: not reported"))

(defun rename-file-and-buffer (newfname)
  "Combine `rename-file' and `rename-buffer' to NEWFNAME.

`set-visited-file-name' does most of the job, but unfortunately
doesn't actually rename the file.  `rename-file' does that, but
doesn't rename the buffer.  `rename-buffer' only renames the buffer,
which is pretty pointless.

Only operates on current buffer because `set-visited-file-name'
also does so and we don't bother doing excursions around."
  (interactive "GRename file and bufer: ")
  (let ((oldfname (buffer-file-name)))
    (set-visited-file-name newfname nil t)
    (rename-file oldfname newfname)
    )
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PERFORMANCE TUNING AND SELF-PROFILING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; deal with binary files opened by mistake and minified files better,
;; by automatically detecting them and optimizing Emacs to behave.
;;
;; now why isn't that the default, I don't know... it looks like
;; `so-long-commentary' has a whole novel about this, and somehow it's
;; not in the Emacs manual, bizarre.
;;
;; cargo-culted from https://www2.lib.uchicago.edu/keith/emacs/#org5116b55
(when (version<= "27.1" emacs-version)  ; only available recently...
  (global-so-long-mode +1))    ; speed up long lines

;; Make gc pauses faster by decreasing the threshold.
;; This is 2 megabytes, default is 800 kilobyte
(setq gc-cons-threshold (* 2 1000 1000))

;; https://github.com/jschaf/esup might help in profiling startup, if
;; it doesn't crash
;; and if it crashes, try this:
;;
;; Work around a bug where esup tries to step into the byte-compiled
;; version of `cl-lib', and fails horribly.
;;(setq esup-depth 0)
;;
;; https://github.com/jschaf/esup/issues/54#issuecomment-651247749

;; from https://blog.d46.us/advanced-emacs-startup/
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     ;; we use this instead of (emacs-init-time)
                     ;; because the latter gives too much precision
                     (format "%.2f seconds"
		             (float-time
		              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;;; keep this comment at the end and add stuff in the previous section
;;; (or better, it the right section!), it will make your merges
;;; happier.

(provide 'anarcat/init)
;;; init.el ends here
