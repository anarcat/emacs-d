;; -*- mode: emacs-lisp; auto-recompile: t; -*-
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-PDF-mode t)
 '(apropos-do-all t)
 '(atomic-chrome-buffer-open-style 'frame)
 '(atomic-chrome-default-major-mode 'markdown-mode)
 '(atomic-chrome-extension-type-list '(ghost-text))
 '(auto-compression-mode t nil (jka-compr))
 '(bar-cursor 2)
 '(blink-cursor-delay 0.3)
 '(blink-cursor-interval 0.3)
 '(bookmark-save-flag 0)
 '(browse-url-browser-function 'browse-url-generic)
 '(browse-url-generic-program "x-www-browser")
 '(buffers-menu-submenus-for-groups-p t)
 '(c-echo-syntactic-information-p t)
 '(calendar-date-style 'iso)
 '(column-number-mode t)
 '(company-go-show-annotation t)
 '(company-tooltip-idle-delay 0.1)
 '(complex-buffers-menu-p nil)
 '(confirm-kill-emacs 'y-or-n-p)
 '(csv-separators '("," "\11" ";"))
 '(custom-file "~/.emacs.d/custom.el")
 '(custom-safe-themes
   '("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa"
     "c5a81a42df109b02a9a68dfe0ed530080372c1a0bbcb374da77ee3a57e1be719"
     "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e"
     "d91ef4e714f05fff2070da7ca452980999f5361209e679ee988e3c432df24347"
     "0598c6a29e13e7112cfbc2f523e31927ab7dce56ebb2016b567e1eff6dc1fd4f"
     "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879"
     "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4"
     "8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26"
     "1297a022df4228b81bc0436230f211bad168a117282c20ddcba2db8c6a200743"
     default))
 '(debian-bug-From-address "Antoine Beaupré <anarcat@debian.org>")
 '(debian-changelog-full-name "Antoine Beaupré")
 '(debian-changelog-mailing-address "anarcat@debian.org")
 '(debug-on-error nil)
 '(default-gutter-position 'bottom)
 '(default-input-method "latin-1-prefix")
 '(default-toolbar-position 'right)
 '(delete-key-deletes-forward t)
 '(delete-selection-mode t)
 '(diff-switches "-u")
 '(display-time-24hr-format t)
 '(efs-use-passive-mode t)
 '(epa-file-name-regexp "\\.\\(gpg\\|pgp\\)\\(~\\|\\.~[0-9]+~\\)?\\'")
 '(epg-debug t)
 '(fortune-file "/home/anarcat/.mutt/sigs.fortune")
 '(fringe-mode '(4 . 4) nil (fringe))
 '(global-font-lock-mode t nil (font-lock))
 '(glyphless-char-display-control '((format-control . acronym) (no-font . hex-code)))
 '(gnus-alias-default-identity "home")
 '(gnus-alias-identity-alist
   '(("home" "" "\"Antoine Beaupré\" <anarcat@orangeseeds.org>" "" nil ""
      fortune-to-signature)
     ("debian" "" "\"Antoine Beaupré\" <anarcat@debian.org>" "Debian"
      nil "" fortune-to-signature)
     ("cat" "" "\"Antoine Beaupré\" <anarcat@anarc.at>" "" nil ""
      fortune-to-signature)
     ("koumbit" "" "\"Antoine Beaupré\" <anarcat@koumbit.org>"
      "Koumbit.org" nil "" fortune-to-signature)
     ("tor" "" "\"Antoine Beaupré\" <anarcat@torproject.org>" "Tor"
      nil "" "Antoine Beaupré\12torproject.org system administration")
     ("lwn" "" "\"Antoine Beaupré\" <lwn@anarc.at>"
      "Linux Weekly News" nil "" "Antoine Beaupré\12LWN.net")))
 '(gnus-alias-identity-rules
   '(("debian" ("any" "@debian\\.org" both) "debian")
     ("koumbit" ("any" "@koumbit\\.org" both) "koumbit")
     ("cat" ("to" "@anarc\\.at" both) "cat")
     ("tor" ("any" "torproject\\.org" both) "tor")
     ("lwn" ("any" "@lwn\\.net\\|lwn@anarc\\.at" both) "lwn")))
 '(gnus-alias-point-position 'empty-header-or-sig)
 '(gnus-nntp-server "news.gmane.io")
 '(gnus-posting-styles '((".*" ("Fcc" "~/sentmail"))))
 '(gnus-secondary-servers '("nntp.lore.kernel.org"))
 '(grep-command "grep -nH -r ")
 '(grep-template "grep <X> <C> -nH -r -e <R> <F>")
 '(gutter-buffers-tab-visible-p nil)
 '(haskell-mode-hook
   '(imenu-add-menubar-index turn-on-haskell-indent
                             turn-on-haskell-unicode-input-method))
 '(html-helper-use-expert-menu t)
 '(indent-tabs-mode nil)
 '(inhibit-startup-echo-area-message "anarcat")
 '(inhibit-startup-screen t)
 '(ispell-dictionary nil)
 '(ledger-reports
   '(("bal" "ledger -f %(ledger-file) bal")
     ("reg" "ledger -f %(ledger-file) reg")
     ("payee" "ledger -f %(ledger-file) reg @%(payee)")
     ("account" "ledger -f %(ledger-file) reg %(account)")
     ("balb" "ledger -f %(ledger-file) bal -B")))
 '(ledger-use-iso-dates t)
 '(line-number-mode t)
 '(log-edit-hook
   '(log-edit-insert-message-template log-edit-insert-cvs-template
                                      log-edit-insert-changelog))
 '(lpr-add-switches nil)
 '(lpr-command "lp")
 '(magit-repolist-columns
   '(("Name" 25 magit-repolist-column-ident nil)
     ("Version" 25 magit-repolist-column-version nil)
     ("B<U" 3 magit-repolist-column-unpulled-from-upstream
      ((:right-align t) (:help-echo "Upstream changes not in branch")))
     ("B>U" 3 magit-repolist-column-unpushed-to-upstream
      ((:right-align t) (:help-echo "Local changes not in upstream")))
     ("flag" 1 magit-repolist-column-flag nil)
     ("Path" 99 magit-repolist-column-path nil)))
 '(mail-envelope-from 'header)
 '(mail-source-delete-incoming nil)
 '(mail-specify-envelope-from t)
 '(make-backup-files nil)
 '(manual-program "LANG=C.UTF-8 man")
 '(menu-accelerator-enabled 'menu-fallback t)
 '(message-auto-save-directory "/home/anarcat/Mail/drafts/")
 '(message-beginning-of-line nil)
 '(message-citation-line-format "On %Y-%m-%d %T, %N wrote:")
 '(message-citation-line-function 'message-insert-formatted-citation-line)
 '(message-dont-reply-to-names '("anarcat@.*" "antoine@koumbit.org" "anarcat+[^@]*@.*"))
 '(message-forward-as-mime t)
 '(message-forward-before-signature nil)
 '(message-forward-show-mml t)
 '(message-kill-buffer-on-exit t)
 '(message-required-mail-headers
   '(From Subject Date (optional . In-Reply-To) Message-ID
          (optional . User-Agent) (optional . Reply-To)))
 '(message-send-mail-partially-limit 20000000)
 '(mml-secure-cache-passphrase nil)
 '(mml-secure-key-preferences
   '((OpenPGP (sign)
              (encrypt
               ("gabster@lelutin.ca"
                "0D47F1D03740F712AA8C630AA28C297118E9F23D")
               ("hiro@torproject.org"
                "DC399D73B442F609261F126D2B4075479596D580")
               ("micah@riseup.net"
                "A40D45ECA1F256046EAAB1113650D3109621C386")
               ("anarcat@torproject.org"
                "BBB6CD4C98D74E1358A752A602293A6FA4E53473")
               ("anarcat@orangeseeds.org"
                "BBB6CD4C98D74E1358A752A602293A6FA4E53473")
               ("mathieu@symbiotic.coop"
                "850E4BF34F115B1BE8CB73159CDC1871E40B0F7A")
               ("eric@openflows.com"
                "75CB21AAC538774611B382BA01C809803B36279F")
               ("nizar@koumbit.org"
                "82E2B7DCD0D8B589375CCCE886569C5B42F8150D")
               ("stoupin@riseup.net"
                "893E64CA4E8BB87C1AA756F05256B77FAAE1DF2D"
                "AB25E55583EE8AFB3EE9F43F710E0CACF82A7359")
               ("dkg@fifthhorseman.net"
                "C4BC2DDB38CCE96485EBE9C2F20691179038E5C6"
                "0EE5BE979282D80B9F7540F1CCD2ED94D21739E9")
               ("ml@nitrane.com"
                "35569024BB83B5468292EEA4ED7E4C1F4F73A298")))
     (CMS (sign) (encrypt))))
 '(mml-secure-openpgp-encrypt-to-self t)
 '(mml-secure-openpgp-signers '("BBB6CD4C98D74E1358A752A602293A6FA4E53473"))
 '(mml-secure-verbose t)
 '(mouse-wheel-scroll-amount '(1 ((shift) . 5) ((control))))
 '(mutt-alias-file-list '("~/.mutt/aliases"))
 '(mwheel-follow-mouse t)
 '(overwrite-mode nil t)
 '(paren-mode 'sexp nil (paren))
 '(require-final-newline t)
 '(safe-local-variable-values
   '((eval c-set-offset 'arglist-close 0)
     (eval c-set-offset 'arglist-intro '++)
     (eval c-set-offset 'case-label 0)
     (eval c-set-offset 'statement-case-open 0)
     (eval c-set-offset 'substatement-open 0)
     (eval when (fboundp 'rainbow-mode) (rainbow-mode 1))
     (c-set-style . "BSD") (python-indent-offset . 4)
     (encoding . utf-8) (js-indent-level . 8) (auto-recompile . t)))
 '(scroll-bar-mode 'right)
 '(scroll-preserve-screen-position 1)
 '(scroll-step 1)
 '(send-mail-function 'sendmail-send-it)
 '(send-pr:datadir "/etc/" t)
 '(show-paren-mode t nil (paren))
 '(size-indication-mode t)
 '(smtpmail-default-smtp-server "submission.anarc.at")
 '(smtpmail-smtp-service 587)
 '(smtpmail-stream-type 'starttls)
 '(text-mode-hook '(turn-on-auto-fill text-mode-hook-identify))
 '(text-scale-mode-step 1.05)
 '(tramp-default-method "ssh")
 '(transient-mark-mode t)
 '(truncate-partial-width-windows nil)
 '(user-mail-address "anarcat@orangeseeds.org")
 '(vc-diff-switches "-u")
 '(vc-initial-comment t)
 '(visible-bell t)
 '(warning-suppress-log-types '((comp) (comp)))
 '(warning-suppress-types '((comp)))
 '(wc-modeline-format "WC[%L%l/%tl %W%w/%tw %C%c/%tc]")
 '(word-wrap t)
 '(x-stretch-cursor t)
 '(yaml-mode-hook '(yaml-set-imenu-generic-expression flycheck-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(anzu-mode-line ((t (:foreground "orange" :weight bold))))
 '(cursor ((t (:background "#990A1B"))))
 '(diff-refine-added ((t (:background "#5f8700"))))
 '(fixed-pitch ((t (:family "Commit mono"))))
 '(fixed-pitch-serif ((t (:family "Commit mono"))))
 '(markdown-comment-face ((t (:foreground "#586e75" :strike-through nil))))
 '(notmuch-search-flagged-face ((t nil)))
 '(notmuch-tag-flagged ((t nil)))
 '(variable-pitch ((t (:family "Charter")))))
