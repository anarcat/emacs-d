;;; early-init --- early customizations
;;; Commentary:
;;
;; Starting from Emacs 27.1, this file is read before the GUI is
;; initialized.  And starting sometime after that and before 29.0.50,
;; this is the *only* place where some things will work.
;;
;; see https://www.gnu.org/software/emacs/manual/html_node/emacs/Early-Init-File.html
;;; Code:

(add-hook 'after-init-hook 'server-start t)

;;; early-init.el ends here
