;;; Package -- notmuch-config
;;
;;; Commentary:
;;
;; This is my notmuch config.  It is auto-loaded by notmuch-emacs.
;;
;;; Code:

; don't remember what that is
(add-hook 'notmuch-show-hook 'visual-line-mode)
; mark sent draft as deleted
(add-hook 'message-send-hook 'notmuch-draft--mark-deleted)

(defun anarcat/autocrypt-insert-header ()
  "Add an autocrypt header for address ADDR in the message.

Requires sequoia to be installed and in the path."
  (let* ((from (or (message-sendmail-envelope-from) user-mail-address))
         (fingerprint (or (car mml-secure-openpgp-signers) from))
         ;; this is not up to spec: there should be a "UID:
         ;; fingerprint" mapping somewhere. autocrypt.el has a
         ;; configured account list we could use, but it adds an extra
         ;; dependency and it's hard to use.
         ;;
         ;; so in this case we trust the keyring to have the right
         ;; key. if there's multiple matching keys, sequoia will
         ;; abort, at least.
         (cmd (format "gpg --export %s | sq autocrypt encode-sender --email %s" fingerprint from)))
    (message "generating Autocrypt with %s..." cmd)
    (message-add-header (string-trim (shell-command-to-string cmd)))))
;; add the Autocrypt header before sending the email
;; XXX: inexplicably broken upstream in 0.39.0
;; https://gitlab.com/sequoia-pgp/sequoia-sq/-/issues/496
;;(add-hook 'message-send-hook 'anarcat/autocrypt-insert-header)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; keymappings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; shortcut keybindings
;;
;; TODO: remove those. we should use notmuch-tag-jump (bound to `k')
;; instead, and notmuch-tagging-keys has been configured accordingly.
(define-key notmuch-show-mode-map "S"
  (lambda ()
    "mark message as spam and advance"
    (interactive nil notmuch-show-mode)
    (notmuch-show-tag '("+spam" "-inbox" "-ham" "-unread" "-flagged"))
    (notmuch-show-next-open-message t)))

(define-key notmuch-search-mode-map "S"
  (lambda (&optional beg end)
    "mark message as spam and advance"
    (interactive (notmuch-search-interactive-region) notmuch-search-mode)
    (notmuch-search-tag (list "+spam" "-inbox" "-ham" "-unread" "-flagged") beg end t)
    (anarcat/notmuch-search-next-thread beg end)))

(define-key notmuch-show-mode-map "H"
  (lambda ()
    "mark message as ham and advance"
    (interactive nil notmuch-show-mode)
    (notmuch-show-tag '("-spam" "-greyspam" "+inbox" "+ham" "+flagged"))
    (notmuch-show-next-open-message t)))

(define-key notmuch-search-mode-map "H"
  (lambda (&optional beg end)
    "mark message as ham and advance"
    (interactive (notmuch-search-interactive-region) notmuch-search-mode)
    (notmuch-search-tag (list "-spam" "-greyspam" "+inbox" "+ham" "+flagged") beg end t)
    (anarcat/notmuch-search-next-thread beg end)))

(define-key notmuch-search-mode-map "u"
  (lambda (&optional beg end)
    "undelete and advance"
    (interactive (notmuch-search-interactive-region) notmuch-search-mode)
    (notmuch-search-tag (list "-deleted") beg end t)
    (anarcat/notmuch-search-next-thread beg end)))

(define-key notmuch-search-mode-map "d"
  (lambda (&optional beg end)
    "delete and advance"
    (interactive (notmuch-search-interactive-region) notmuch-search-mode)
    (notmuch-search-tag (list "+deleted" "-unread") beg end t)
    (anarcat/notmuch-search-next-thread beg end)))

(define-key notmuch-show-mode-map "d"
  (lambda ()
    "delete current message and advance"
    (interactive nil notmuch-show-mode)
    (notmuch-show-tag '("+deleted" "-unread"))
    (notmuch-show-next-open-message t)))

(define-key notmuch-show-mode-map "T"
  (lambda ()
    "unmark message as todo"
    (interactive nil notmuch-show-mode)
    (notmuch-show-tag '("-todo"))))
(define-key notmuch-show-mode-map "t"
  (lambda ()
    "mark message as todo"
    (interactive nil notmuch-show-mode)
    (notmuch-show-tag '("+todo"))))
(define-key notmuch-search-mode-map "T"
  (lambda (&optional beg end)
    "unmark threads as todo, no lowercase t equivalent because that
is notmuch-search-filter-by-tag"
    (interactive (notmuch-search-interactive-region) notmuch-search-mode)
    (notmuch-search-tag (list "-todo") beg end t)
    (anarcat/notmuch-search-next-thread beg end)))

;; https://notmuchmail.org/emacstips/#index17h2
(define-key notmuch-show-mode-map "b"
  (lambda (&optional address)
    "Bounce the current message."
    (interactive "sBounce To: " notmuch-show-mode)
    (notmuch-show-view-raw-message)
    (message-resend address)
    (kill-buffer)))

;; override message-mode's "postpone" by notmuch
(define-key message-mode-map (kbd "C-c C-d") 'notmuch-draft-postpone)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; my custom notmuch functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun anarcat/notmuch-search-next-thread (&optional beg end)
  "Skip to next message from region (BEG END) or point.

This is necessary because notmuch-search-next-thread just starts
from point, whereas it seems to me more logical to start from the
end of the region."
  ;; move line before the end of region if there is one
  (unless (= beg end)
    (goto-char (- end 1)))
  (notmuch-search-next-thread))

;; Linking to notmuch messages from org-mode
;; https://notmuchmail.org/emacstips/#index23h2
(require 'org-notmuch nil t)
(autoload 'gnus-alias-determine-identity "gnus-alias" "" t)
(add-hook 'message-setup-hook 'gnus-alias-determine-identity)

(defun notmuch-address-complete-at-point ()
  "Complete the address using `notmuch-address-command'.

This replaces the old `notmuch-address-expand-name' with the new
`completion-at-point-functions' (capf) system that's compatible
with corfu, company and friends."
  (when notmuch-address-command
    (let* ((end (point))
	   (beg (save-excursion
		  (re-search-backward "\\(\\`\\|[\n:,]\\)[ \t]*")
		  (goto-char (match-end 0))
		  (point)))
	   (orig (buffer-substring-no-properties beg end))
	   (completion-ignore-case t)
	   (options (with-temp-message "Looking for completion candidates..."
		      (notmuch-address-options orig))))
      (list beg end options))))

;; override `notmuch-address-expand-name' from `notmuch-address-setup'
;;
;; disabled, it doesn't work reliably, probably because of timeout
;; issues.
;;
;;;(cl-pushnew (cons notmuch-address-completion-headers-regexp
;;;		  #'notmuch-address-complete-at-point)
;;;	    message-completion-alist :test #'equal)

(defun anarcat/notmuch-html-convert ()
  "Create an HTML part from a Markdown body.

This will not work if there are *any* attachments of any form, those should be added after."
  (interactive)
  (save-excursion
    ;; fetch subject, it will be the HTML version title
    (message "building HTML attachment...")
    (message-goto-subject)
    (beginning-of-line)
    (search-forward ":")
    (forward-char)
    (let ((beg (point))) (end-of-line)
         (let ((subject (buffer-substring beg (point)))
               (output-buffer-name "*notmuch-markdown-output*"))
           (message "determined title is %s..." subject)
           ;; wrap signature in a <pre>
           (message-goto-signature)
           (forward-line -1)
           ;; save and delete signature which requires special formatting
           (let ((signature (buffer-substring (point) (point-max))))
             (delete-region (point) (point-max))
             ;; set region to top of body then end of buffer
             (goto-char (point-max))
             (message-goto-body)
             (narrow-to-region (point) (mark))
             ;; run markdown on region
             (message "running markdown...")
             (markdown output-buffer-name)
             (widen)
             (with-current-buffer output-buffer-name
               (goto-char (point-max))
               ;; add signature formatted as <pre>
               (insert "\n<pre>")
               (insert signature)
               (insert "</pre>\n")
               (markdown-add-xhtml-header-and-footer subject))
             (message "done the dirty work, re-inserting everything...")
             ;; restore signature
             (message-goto-signature)
             (insert signature)
             (message-goto-body)
             (insert "<#multipart type=alternative>\n")
             (goto-char (point-max))
             (insert "<#part type=text/html>\n")
             (insert-buffer-substring output-buffer-name)
             (goto-char (point-max))
             (insert "<#/multipart>\n")
             (let ((f (buffer-size (get-buffer output-buffer-name))))
               (message "appended HTML part (%s bytes)" f)))))))

(defvar anarcat-work-search-terms
  "(from:*@torproject.org or to:*@torproject.org or tag:tor or tag:planet-puppet or tag:rt)"
  "Search pattern matching email from work.")

;; this, and all of notmuch-tag, has become a complete mess.
;;
;; the proper way to handle this would be to use notmuch queries and,
;; in the case of work/home distinction, a macro to redact that search term.
;;
;; this could also replace the "all tags" thing with queries that are
;; *automatically* loaded here. we could have a small set of hardcoded
;; ones (but those could be queries as well!) and everything else
;; could be loaded dynamically from the output of `notmuch config list
;; | grep ^query`
(defvar anarcat-saved-searches-home
  `((:name "inbox"        :key "I" :query ,(concat "not " anarcat-work-search-terms " and tag:inbox"))
    (:name "unread inbox" :key "i" :query ,(concat "not " anarcat-work-search-terms " and tag:inbox and tag:unread"))
    (:name "unread"       :key "u" :query ,(concat "not " anarcat-work-search-terms " and tag:unread"))
    (:name "sent"         :key "s" :query ,(concat "not " anarcat-work-search-terms " and tag:sent and date:30d.."))
    (:name "drafts"       :key "d" :query ,(concat "not " anarcat-work-search-terms " and (tag:draft or tag:Drafts)"))
    (:name "r&r" :query ,(concat "not " anarcat-work-search-terms "and not tag:rapports and not (subject:pipeline and tag:gitlab) and not tag:lists and not tag:feeds and date:12months.. and not tag:riots96 and not tag:communauto"))
    (:name "todo"         :key "t" :query ,(concat "not " anarcat-work-search-terms " and tag:todo")))
  "Saved searches for home.")

(defvar anarcat-saved-searches-work
  ;; we use append here but we could also keep this list work-specific
  ;; and use add-to-list with a predicate comparing (say) :key or
  ;; :name to allow *overrides* from work. that would be done directly
  ;; over saved_searches in -off/-on
  (append
   anarcat-saved-searches-home
   `((:name "work inbox"  :key "W" :query ,(concat anarcat-work-search-terms " and tag:inbox"))
     (:name "work unread" :key "w" :query ,(concat anarcat-work-search-terms " and tag:unread"))
     (:name "work sent"   :key "S" :query ,(concat anarcat-work-search-terms " and tag:sent date:30d.."))
     (:name "work and home inbox unread" :key "i" :query "tag:inbox and tag:unread")
     (:name "work and home unread" :key "u" :query "tag:unread")
     (:name "work sent"   :key "s" :query "tag:sent date:30d..")
     (:name "work drafts" :key "D" :query ,(concat anarcat-work-search-terms " and tag:draft or tag:Drafts"))
     (:name "work todo"   :key "T" :query ,(concat anarcat-work-search-terms " and tag:todo"))
     (:name "r&r" :query ,(concat anarcat-work-search-terms "and not tag:rapports and not (subject:pipeline and tag:gitlab) and not tag:lists and not tag:feeds and date:12months.. and not tag:riots96 and not tag:communauto"))
     (:name "vegas"       :key "v" :query ,(concat anarcat-work-search-terms " and date:thu.. and not tag:rapports") :sort-order oldest-first)))
   "Saved searches for work.")

(defun anarcat/notmuch-work-off ()
  "Disable work tags in all tags view.

See also `anarcat/notmuch-work-on'.

This could also censor the saved searches.  The are a few ways
that could be done:

 1. define the list of work and non-work searches elsewhere, and
 join them here

 2. iterate over the saved searches and hijack the :count-query
    to return zero for some searches

 3. tweak a `notmuch-saved-search-sort-function' to remove the
 work saved searches

 4. `notmuch-hello-query-counts' does check a :filter option, but
    it's not passed by `notmuch-hello-insert-saved-searches', so
    that would require a new variable to be passed, a bit like
    `notmuch-show-empty-saved-searches', maybe
    `notmuch-saved-searches-filter'?"
  (interactive)
  (setq notmuch-hello-tag-list-make-query (concat "tag:unread and not tag:killed and not " anarcat-work-search-terms))
  (setq notmuch-saved-searches anarcat-saved-searches-home)
  (notmuch-refresh-this-buffer))

(defun anarcat/notmuch-work-on ()
  "Disable work tags in all tags view.

See also `anarcat/notmuch-work-on'."
  (interactive)
  (setq notmuch-hello-tag-list-make-query "tag:unread and not tag:killed")
  (setq notmuch-saved-searches anarcat-saved-searches-work)
  (notmuch-refresh-this-buffer))

;; check hostname and enable or disable work based on the host
(let
    ;; this extracts the hostname without a trailing newline
    ((hostname (car (split-string (shell-command-to-string "hostname")))))
  (message "checking hostname %s for saved search" hostname)
  (when (string-equal hostname "REMOTE-HOSTNAME") ; disabled
    (setq notmuch-command "notmuch-remote")
    ;; enable gpg decryption in raw view
    (defadvice notmuch-show-view-raw-message
        (after notmuch-show-view-raw-message-after activate)
      (epa-mail-mode)))
  (cond ((or
          (string-equal hostname "angela")
          (string-equal hostname "emma")
          (string-equal hostname "marcos"))
         (message "on %s, we do not work" hostname)
         (anarcat/notmuch-work-off))
        ((or
          (string-equal hostname "curie"))
         (message "on %s, we work" hostname)
         (anarcat/notmuch-work-on))
        (t
         (warn "not sure what to do about saved searches on %s" hostname))))

(defun anarcat/notmuch-disable-buttons ()
  "Disable the 'multipart', 'citation' and 'signature' buttons.

  That makes it easier to copy-paste emails in their entirety."
  (interactive nil notmuch-show-mode)
  (setq notmuch-show-insert-header-p-function 'notmuch-show-reply-insert-header-p-never)
  (customize-set-value 'notmuch-wash-citation-lines-prefix 10000000000)
  (customize-set-value 'notmuch-wash-signature-lines-max 10000000000))

(defun anarcat/notmuch-enable-buttons ()
  "Re-enable the 'multipart', 'citation' and 'signature' buttons.

  That makes it easier to copy-paste emails in their entirety."
  (interactive nil notmuch-show-mode)
  (setq notmuch-show-insert-header-p-function 'notmuch-show-insert-header-p)
  (custom-reevaluate-setting 'notmuch-wash-citation-lines-prefix)
  (custom-reevaluate-setting 'notmuch-wash-signature-lines-max))

;; Debian package elpa-message-templ
(use-package message-templ
  :bind (("C-c s" . message-templ-select))
  :config
  (setq message-templ-alist
        '(("tpa-meeting-invite"
           ("From" . "Antoine Beaupré <anarcat@torproject.org>")
           ("To" . "TorProject.org admins <torproject-admin@torproject.org>")
           ("Subject" . "team meeting 1500UTC ... 2020")
           (body . "
Hi!

We're meeting this Monday, ... at 1500UTC.

Equivalent to: 08:00 US/Pacific, 11:00 US/Eastern, 15:00 UTC,
17:00 Europe/Paris.

The meeting should be an hour long and will cover those topics:

# Roll call: who's there and emergencies
# Roadmap review
# Other discussions
# Next meeting

The agenda is also available here:

https://pad.riseup.net/...

If there are other points you want to bring up do let me know before the
meeting so I can plan and manage time accordingly.

The meeting will be held in the #tpo-admin channel on the irc.OFTC.net
IRC network.

Thanks, and see you monday!
")))))

;; make "click this email" pop notmuch-message instead of plain message-mode
;;
;; this fixes the "aaargh where's my Fcc!" problem
(setq mail-user-agent 'notmuch-user-agent)

(message "anarcat's custom notmuch config loaded")
(provide 'anarcat-notmuch-config)
;;; notmuch-config.el ends here
