(defun find-url-at-point (&optional url)
  "Open the URL at point."
  (interactive)
  (switch-to-buffer
   (url-retrieve-synchronously
    (or url (let ((default-search-string (if (region-active-p) (buffer-substring-no-properties (region-beginning) (region-end)) (thing-at-point-url-at-point) )))
            (read-string (format "URL: %s" (or default-search-string ""))  nil nil default-search-string) ) )) ))
