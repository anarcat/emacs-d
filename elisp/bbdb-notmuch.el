; capture addresses 
(defun bbdb/notmuch-snarf-header (header)
  (let ((text (notmuch-show-get-header header)))
    (with-temp-buffer
      (insert text)
      (bbdb-snarf-region (point-min) (point-max)))))

(defun bbdb/notmuch-snarf-from ()
  (interactive)
  (bbdb/notmuch-snarf-header :From))

(defun bbdb/notmuch-snarf-to ()
  (interactive)
  (bbdb/notmuch-snarf-header :To))

(define-key notmuch-show-mode-map "b" 'bbdb/notmuch-snarf-from)
(define-key notmuch-show-mode-map "B" 'bbdb/notmuch-snarf-to)

;; color from line according to known / unknown sender
; code taken from bbdb-gnus.el
(defun bbdb/notmuch-known-sender ()
  (let* ((from (plist-get headers :From))
         (splits (mail-extract-address-components from))
         (name (car splits))
         (net (cadr splits))
         (record (and splits
                      (bbdb-search-simple
                       name
                       (if (and net bbdb-canonicalize-net-hook)
                           (bbdb-canonicalize-address net)
                         net)))))
    (and record net (member (downcase net) (bbdb-record-net record)))))

(defun bbdb/check-known-sender ()
  (interactive)
  (if (bbdb/notmuch-known-sender) (message "Sender is known") (message "Sender is not known")))

(defface notmuch-show-known-addr
  '(
    (((class color) (background dark)) :foreground "spring green")
    (((class color) (background light)) :background "spring green" :foreground "black"))
  "Face for sender or recipient already listed in bbdb"
  :group 'notmuch-show
  :group 'notmuch-faces)

(defface notmuch-show-unknown-addr
  '(
    (((class color) (background dark)) :foreground "dark orange")
    (((class color) (background light)) :background "gold" :foreground "black"))
  "Face for sender or recipient not listed in bbdb"
  :group 'notmuch-show
  :group 'notmuch-faces)

;;; XXX this needs to be reimplemented using the hooks in notmuch-show-markup-headers-hook
;; ; override function from notmuch-show
;; (defun notmuch-show-insert-headerline (headers date tags depth)
;;   "Insert a notmuch style headerline based on HEADERS for a
;; message at DEPTH in the current thread."
;;   (let ((start (point))
;;         (face (if (bbdb/notmuch-known-sender) 'notmuch-show-known-addr 'notmuch-show-unknown-addr))
;;         (end-from))
;;     (insert (notmuch-show-spaces-n (* notmuch-show-indent-messages-width depth))
;; 	    (notmuch-show-clean-address (plist-get headers :From)))
;;     (setq end-from (point))
;;     (insert
;; 	    " ("
;; 	    date
;; 	    ") ("
;; 	    (propertize (mapconcat 'identity tags " ")
;; 			'face 'notmuch-tag-face)
;; 	    ")\n")
;;     (overlay-put (make-overlay start (point)) 'face 'notmuch-message-summary-face)
;;     (save-excursion
;;       (goto-char start)
;;       (overlay-put (make-overlay start end-from) 'face face))))

(provide 'bbdb-notmuch)
