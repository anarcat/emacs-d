;;; $Id: dw.el,v 1.34 2002-10-24 18:24:40 anarcat Exp $
;;;
;;; this file is free to distribute and copy
;;; 
;;; DreamWeaver template engine for [X]Emacs. In your face, Macromedia!
;;;
;;; Copyright 2002 The Anarcat <anarcat@anarcat.ath.cx>
;;;
;;; TODO:
;;;
;;; - links updates
;;; - site support (this module can find the Template only if in .)
;;; - write the created buffer somewhere

(defun fill-dw-template (&optional buf)
  "Update a file using a DreamWeaver Template"
  (interactive)
  (or buf
      (setq buf (current-buffer)))
  (save-excursion
    (set-buffer buf)
    (goto-char (point-min))
    ;; DW-controlled file have a template declaration somewhere on top
    (if (search-forward-regexp instance-begin-regexp nil 'not-nil-t)
        ;; XXX: this works only when all files are in current directory
        (let ((template-path 
	       (concat "." (buffer-substring (match-beginning 1)
					     (match-end 1))))
	      (instance-buffer (generate-new-buffer "*instance-buffer*"))
	      (leading-instance (buffer-substring (point-min) (match-end 0)))
	      )
	  
	  (message "template path: %s" template-path)
	  
	  (save-excursion
	    (set-buffer instance-buffer)
	    (insert-file-contents template-path)
	    )
	  (goto-char (point-min instance-buffer) instance-buffer)

	  ;; iterate over all editable regions
	  (while (search-forward-regexp template-begin-regexp nil
					'not-nil-t nil instance-buffer)
	    (let ((m (match-data))
		  (region-name (buffer-substring (match-beginning 1)
						 (match-end 1)
						 instance-buffer))
		  (region-start (match-beginning 0)))
	      
	      (if (search-forward-regexp template-end-regexp nil 
					 'not-nil-t nil instance-buffer)
		  (let ((region-end (match-end 0)))
		    (save-excursion
		      (set-buffer instance-buffer)
		      (delete-region region-start region-end)
		      )
		    (if (search-forward-regexp (format instance-begin-editable-format region-name)
					       nil 'not-nil-t)
			(let ((instance-beg (match-beginning 0)))
			  (if (search-forward-regexp instance-end-editable-format nil 'not-nil-t)
			      (insert-string (buffer-substring instance-beg (match-end 0))
					     instance-buffer)
			    (message "not found")
			    )
			  )
		      (insert (format instance-begin-editable-format region-name)
			      "not found!!!"
			      instance-end-editable-format)
		      )
		    )
		(message "incomplete editable region (%s) skipping"
			 region-name))
	      )
	    )
	  (if (search-forward-regexp instance-end-regexp nil 'not-nil-t nil)
	      (let ( (trailing-instance (buffer-substring (match-beginning 0) (point-max))) )
		(if (search-backward-regexp "</html>" nil 'not-nil-t nil instance-buffer)
		    (save-excursion
		      (set-buffer instance-buffer)
		      (delete-region (match-beginning 0) (point-max))
		      (goto-char (point-max))
		      (insert trailing-instance)
		      )
		  (message "can't find closing html tag")
		  )
		)
	    (message "can't find end of template instance")
	    )
	  (if (search-backward-regexp "<html>" nil 'not-nil-t nil instance-buffer)
	      (save-excursion
		(set-buffer instance-buffer)
		(delete-region (point-min) (match-end 0))
		(goto-char (point-min))
		(insert leading-instance)
		)
	    (message "html tag not found in doc")
	    )
	  
	  ;; XXX: we will have to write-file this buffer into a real file
	  
	  )
      (message "template declaration not found in buffer"))
    )
  )

;; not sure we should make those customized, but oh well
(defgroup DreamWeaver nil
  "DreamWeaver template support for emacs"
  :group 'applications)

(defgroup Regexps nil
  "DreamWeaver osbcure regular expressions you shouldn't mess with"
  :group 'DreamWeaver)

(defcustom instance-begin-regexp
  "<!-- InstanceBegin template=\"\\(.*\\)\" *codeOutsideHTMLIsLocked=\"\\(.*\\)\" -->"
  "The start tag of a template declaration. You shouldn't need to edit this."
  :type 'string
  :group 'Regexps)

(defcustom instance-end-regexp
  "<!-- InstanceEnd -->"
  "The end of a template instance. You shouldn't need to edit this."
  :type 'string
  :group 'Regexps)

(defcustom instance-begin-editable-format
  "<!-- InstanceBeginEditable name=\"%s\" -->"
  "The start tag of an editable region instance. You shouldn't need to edit this.

WARNING: this must contain a %s or `format' will crash"
  :type 'string
  :group 'Regexps)

(defcustom instance-end-editable-format
  "<!-- InstanceEndEditable -->"
  "The end tag of an editable region instance. You shouldn't need to edit this."
  :type 'string
  :group 'Regexps)

(defcustom template-begin-regexp
  "<!-- *TemplateBeginEditable name=\"\\(.*\\)\" -->"
  "The start of an editable region template. You shouldn't need to edit this."
  :type 'string
  :group 'Regexps)

(defcustom template-end-regexp
  "<!-- *TemplateEndEditable -->"
  "The end of an editable region template. You shouldn't need to edit this."
  :type 'string
  :group 'Regexps)
